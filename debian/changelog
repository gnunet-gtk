gnunet-gtk (0.23.1) unstable; urgency=low

  * Package for GNUnet v0.23.1.

 -- Christian Grothoff <grothoff@gnu.org>  Mon, 16 Dec 2024 21:51:13 +0200

gnunet-gtk (0.22.0) unstable; urgency=low

  * Package for GNUnet v0.22.0.

 -- Christian Grothoff <grothoff@gnu.org>  Fri, 28 Aug 2024 21:51:13 +0200

gnunet-gtk (0.21.0-2) unstable; urgency=low

  * Package for GNU Taler v0.10.0.

 -- Christian Grothoff <grothoff@gnu.org>  Fri, 12 Apr 2024 03:50:12 +0200

gnunet-gtk (0.21.0-1) unstable; urgency=low

  * Package for GNU Taler v0.9.4.

 -- Christian Grothoff <grothoff@gnu.org>  Sat, 10 Feb 2024 03:50:12 +0200

gnunet-gtk (0.21.0) unstable; urgency=low

  * First work towards packaging v0.21.0.

 -- Christian Grothoff <grothoff@gnu.org>  Thu, 7 Dec 2023 23:50:12 +0200

gnunet-gtk (0.20.0) unstable; urgency=low

  * First work towards packaging v0.20.0.

 -- Christian Grothoff <grothoff@gnu.org>  Thu, 7 Sep 2023 23:50:12 +0200

gnunet-gtk (0.19.2-1) unstable; urgency=low

  * Synchronizing release versions, bumping version.

 -- Christian Grothoff <grothoff@gnu.org>  Tue, 17 Jan 2023 22:14:18 +0200

gnunet-gtk (0.19.2) unstable; urgency=low

  * New upstream release, bumping version.

 -- Christian Grothoff <grothoff@gnu.org>  Tue, 17 Jan 2023 21:14:18 +0200

gnunet-gtk (0.18.1) unstable; urgency=low

  * New upstream release, bumping version.

 -- Christian Grothoff <grothoff@gnu.org>  Sat, 04 Nov 2022 21:14:18 +0200

gnunet-gtk (0.17.0) unstable; urgency=low

  * New upstream release, bumping version.

 -- Christian Grothoff <grothoff@gnu.org>  Mon, 20 Jun 2022 21:14:18 +0200

gnunet-gtk (0.15.0) unstable; urgency=low

  * New upstream release, bumping version.

 -- Christian Grothoff <grothoff@gnu.org>  Sat, 28 Aug 2021 21:14:18 +0200

gnunet-gtk (0.14.1-3) unstable; urgency=low

  * fix NLS logic of libgnunetgtk, avoiding broken Gtk+ API

 -- Christian Grothoff <grothoff@gnu.org>  Wed, 14 Jul 2021 19:23:15 +0200

gnunet-gtk (0.14.1-2) unstable; urgency=low

  * split off libgnunetgtk package for shared library
  * renamed gnunet-gtk-dev to libgnunetgtk-dev

 -- Christian Grothoff <grothoff@gnu.org>  Fri, 18 Jun 2021 19:23:15 +0200

gnunet-gtk (0.14.1-1) unstable; urgency=low

  * import into upstream Git repository from salsa.debian.org
  * updated to latest release
  * modified upstream to minimize patching, removing patches applied upstream

 -- Christian Grothoff <grothoff@gnu.org>  Fri, 18 Jun 2021 11:23:15 +0200

gnunet-gtk (0.13.1-1) unstable; urgency=medium

  * debian/upstream/signing-key.asc: reexport upstream signing key with
    minimal options.
  * Add ng0's and Martin Schanzenbach's gpg keys to
    debian/upstream/signing-key.asc.
  * Remove debian/patches/typos.diff, included upstream.
  * Refresh patches with gbp pq.
  * debian/copyright:
    + update years.
    + distribute debian/* under GPL-3+ instead of GPL-2+.
  * Remove pre-oldstable dbgsym-migration in debian/rules.
  * Apply debhelper-compat 13:
    + declare build-dependency on debhelper-compat (=13).
    + remove build-dependency on dh-autoreconf.
    + remove debian/compat.
    + remove dh_install --fail-missing from debian/rules.
    + remove --with-autoreconf from debian/rules.
  * Simplify debian/rules with execute_after_dh_auto_install.
  * Use wrap-and-sort -as.
  * debian/control:
    + remove breaks and replace from pre-oldstable.
    + use https in Homepage URI.
    + bump the minimal gnunet version to 0.13.1.
  * Standards-version: 4.5.0, no changes needed.
  * Remove debian/man/gnunet-conversation-gtk.1, included upstream.

 -- Bertrand Marc <bmarc@debian.org>  Wed, 15 Jul 2020 16:33:01 +0200

gnunet-gtk (0.11.0-1) experimental; urgency=medium

  * New upstream version 0.11.0.
  * Refresh debian/patches/ac_init_version.diff.
  * Standards-version: 4.3.0, no changes needed.
  * debian/upstream/signing-key.asc: reexport upstream signing key with minimal
    options.

 -- Bertrand Marc <bmarc@debian.org>  Sun, 03 Mar 2019 14:53:43 +0100

gnunet-gtk (0.11.0~pre66-1) experimental; urgency=medium

  * Update debian/watch to version 4, add a rule deal with pre and rc versions.
  * Move the package to salsa and update Vcs-browser and Vcs-git accordingly.
  * Add Christian Grothoff's new key to debian/upstream/signing-key.asc.
  * New upstream version 0.11.0~pre66
  * debian/patches:
    + Refresh dont_copy_license.diff and typos.diff.
    + Delete reproducible_build.diff, included upstream.
    + Fix a typo in dont_copy_license.diff's description.
  * Update build-dependencies according to README:
    + gnunet-dev (>= 0.11.0~).
    + libgtk-3-dev (>=3.22.0).
  * debian/source/options: remove custom compression options.
  * debian/copyright: use https in the copyright-format URL.
  * Standard-version: 4.2.1.

 -- Bertrand Marc <bmarc@debian.org>  Sat, 10 Nov 2018 17:28:34 +0100

gnunet-gtk (0.10.1-5) unstable; urgency=medium

  * Move the plugins from gnunet-gtk-dev to gnunet-gtk and add
    breaks/replaces: gnunet-gtk-dev for gnunet-gtk (Closes: #855706).

 -- Bertrand Marc <bmarc@debian.org>  Tue, 28 Feb 2017 20:01:19 +0100

gnunet-gtk (0.10.1-4) unstable; urgency=medium

  * Add a patch written by Chris Lamb to make the build reproducible
    (Closes: #834111).
  * debian/copyright: move to format 1.0 and update years and license.
  * Add missing dependencies for gnunet-gtk-dev.
  * Use my @debian.org address for the maintainer field.
  * Migrate from a manual "-dbg" package to an automatic generated debug
    symbol package:
    + debian/control: remove gnunet-gtk-dbg.
    + debian/rules: use --dbgsym-migration in override_dh_strip.

 -- Bertrand Marc <bmarc@debian.org>  Sat, 20 Aug 2016 13:04:30 +0200

gnunet-gtk (0.10.1-3) unstable; urgency=medium

  * Switch from gksu to policykit (Closes: #822602):
    + Remove the dependency on gksu.
    + Add a dependency on policykit-1.
    + Rewrite the wrapper around gnunet-setup to use pkexec and rename it
      gnunet-setup-pkexec.
    + Create a link to the gnunet-setup manpage for gnunet-setup-pkexec.
    + Install gnunet-setup in /usr/bin.
    + Set a policy kit file for gnunet-setup.
    + Patch gnunet-setup.desktop to launch gnunet-setup-pkexec.
  * debian/control: use secure URI for the Vcs-* fields.
  * Standards version: 3.9.8.
  * Fix another typo in debian/patches/typos.diff.
  * Switch to debhelper v9:
    + Bump debian/compat.
    + Build-depend on debhelper (>= 9).
    + debian/rules: replace usr/lib/ with usr/lib/$(DEB_HOST_MULTIARCH) and
      stop using dpkg-buildflags in dh_auto_configure.
    + debian/*.install: replace usr/lib/ with usr/lib/*/.

 -- Bertrand Marc <beberking@gmail.com>  Sun, 08 May 2016 16:10:26 +0200

gnunet-gtk (0.10.1-2) unstable; urgency=medium

  * Put the upstream signing key in debian/upstream/signing-key.asc and
    remove debian/source/include-binaries.
  * debian/control: update homepage as www.gnunet.org does not work anymore.
  * debian/control: update description according to upstream website.
  * Recommends gnunet instead of suggesting depreciated gnunet-server and
    fix long description (Closes: #768042).
  * Remove gnunet-gtk.menu and its icon in debian/pixmaps. Update
    gnunet-gtk.install accordingly.

 -- Bertrand Marc <beberking@gmail.com>  Thu, 22 Oct 2015 19:37:45 +0200

gnunet-gtk (0.10.1-1) unstable; urgency=medium

  * Imported Upstream version 0.10.1
  * debian/control:
    + build-depend on gnunet-dev (>= 0.10).
    + make gnunet-gtk depend on ${shlibs:depends}.
  * Change the name of the service in the gnunet-setup wrapper.
  * Refresh patches.
  * Add the new files to gnunet-gtk*.install.
  * debian/rules: fix the case of the directory gnunet to remove properly .la
    files.
  * Remove peerinfo-gtk, setup and statistics-gtk manpages, they are included
    and upgraded upstream.
  * Delete debian/gnunet-gtk.manpages, not necessary.
  * Add a patch to fix a few typos in gnunet-setup manpage.
  * Add minimal manpages for gnunet-gtk and gnunet-conversation-gtk.
  * Standards version 3.9.6: no changes needed.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 04 Oct 2014 15:04:48 +0200

gnunet-gtk (0.9.5-1) unstable; urgency=medium

  * Make uscan check the archive signature:
    + add the upstream key to debian/upstream-signing-key.pgp.
    + add the binary key to debian/source/include-binaries.
    + add pgpsigurlmangle to debian/watch.
  * Imported Upstream version 0.9.5
  * Move the git repository to collab-maint.
  * debian/gnunet-gtk.install: remove gnunet-gns-gtk, it is now integrated with
    gnunet-setup.
  * Standards version 3.9.5: no changes needed.
  * Do not install the licence file.
  * Build-depend on libqrencode.
  * Do not build-depend on guile-1.8-dev, it is not used in any way.
  * Build-depend on gnunet-dev (>=0.9.5) as it fails to build with a lower
    version.

 -- Bertrand Marc <beberking@gmail.com>  Tue, 11 Feb 2014 20:35:43 +0100

gnunet-gtk (0.9.3-1) unstable; urgency=low

  * Imported Upstream version 0.9.3
  * Install gnunet-gns-gtk.
  * Depends on gnunet (>= 0.9.3) as it will fail to build with any lower
    version.
  * Pass hardening build flags to configure.
  * Add a short manpage for gnunet-gns-gtk.

 -- Bertrand Marc <beberking@gmail.com>  Tue, 19 Jun 2012 20:37:30 +0200

gnunet-gtk (0.9.2-2) unstable; urgency=low

  * debian/control: add Vcs-Git and Vcs-browser fields.
  * Gnunet-gtk breaks/replaces gnunet-tools (<<0.9) as they both provide
    gnunet-setup (Closes: #673802).

 -- Bertrand Marc <beberking@gmail.com>  Wed, 23 May 2012 22:10:21 +0200

gnunet-gtk (0.9.2-1) unstable; urgency=low

  * New maintainer (Closes: #660441).
  * New upstream release 0.9.2.
  * Add debian/watch.
  * Remove the only patch: applied.
  * Move to autoreconf:
    + build-depends on autoconf, automake, autopoint, dh-autoreconf.
    + debian/rules: use dh --with autoreconf.
    + patch configure.ac to pass GNUnet GTK version to autoreconf.
  * Standards version 3.9.3: no changes needed.
  * The main GTK client is now gnunet-fs-gtk:
    + update gnunet-gtk.manpages
    + update gnunet-gtk.menu
    + update icon symlink in gnunet-gtk.links.
  * debian/copyright: GNUnet is now released under GPL3.
  * Add minimal man pages.
  * debian/rules: remove unrecognize options to configure: libgku2 libnotify.
  * Update build-dependencies:
  	+ remove libadns1-dev, libgksu2-dev, libgcrypt11-dev, libgmp3-dev,
  	  libnotify-dev, librsvg2-dev, libextractor-dev.
  	+ update libgtk to libgtk-3-dev.
  * Add a wrapper around gnunet-setup and depends on gksu.

 -- Bertrand Marc <beberking@gmail.com>  Sat, 21 Apr 2012 12:28:36 +0200

gnunet-gtk (0.8.1a-5) unstable; urgency=low

  * Orphaning package.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Sun, 19 Feb 2012 08:44:38 +0000

gnunet-gtk (0.8.1a-4) experimental; urgency=low

  * Updating maintainer and uploaders fields.
  * Removing vcs fields.
  * Updating to debhelper version 8.
  * Updating to standards version 3.9.2.
  * Removing references to my old email address.
  * Switching to source version 3.0 (quilt).
  * Simplyfing autotools handling in rules.
  * Removing leading slash in debhelper install files.
  * Moving installation of pixmap files from rules to debhelper install
    file.
  * Making packaging distribution neutral.
  * Updating years in copyright file.
  * Compacting copyright file.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Thu, 15 Sep 2011 12:43:07 +0200

gnunet-gtk (0.8.1a-3) unstable; urgency=low

  [ Daniel Baumann ]
  * Updating standards version to 3.9.0.

  [ Jérémy Bobbio ]
  * Uploaded at Daniel Baumann's request.
  * Add compatibility with libnotify 0.7 API. (Closes: #630279)

 -- Jérémy Bobbio <lunar@debian.org>  Thu, 21 Jul 2011 15:31:15 +0200

gnunet-gtk (0.8.1a-2) unstable; urgency=medium

  * Updating versioned gnunet depends to 0.8.1a.
  * Rebuilding against libextractor with epoche to ease testing
    migration.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Sat, 20 Mar 2010 12:15:51 +0100

gnunet-gtk (0.8.1a-1) unstable; urgency=low

  * Correcting upstream homepage in copyright.
  * Updating year in copyright file.
  * Updating to standards 3.8.4.
  * Merging upstream version 0.8.1a.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Mon, 15 Feb 2010 13:14:05 +0100

gnunet-gtk (0.8.1-1) unstable; urgency=low

  * Removing icons.patch, not required anymore.
  * Symlinking icon rather than installing another copy of it.
  * Adding a patch to correct typo in upstream desktop file, and
    dropping the local one.
  * Removing not required sourcedir parameter from dh_install override.
  * Updating to standards version 3.8.3.
  * Adding maintainer homepage field to control.
  * Marking maintainer homepage field to be also included in binary
    packages and changelog.
  * Adding README.source.
  * Simplifying autotools handling in rules.
  * Moving maintainer homepage field from control to copyright.
  * Updating REAMDE.source.
  * Dropping la files.
  * Bumping versioned build-depends on debhelper.
  * Bumping versioned build-depends on quilt.
  * Adding explicit source version 1.0 until switch to 3.0.
  * Merging upstream version 0.8.1.
  * Removing desktop.patch, merged upstream.
  * Updating versioned gnunet depends to 0.8.1.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Tue, 29 Dec 2009 18:52:46 +0100

gnunet-gtk (0.8.0c-2) unstable; urgency=low

  * Removing unneded versions for build-depends.
  * Updating maintainer field.
  * Updating vcs fields.
  * Wrapping lines in control.
  * Updating package to standards version 3.8.2.
  * Simplify gnunet-gtk-dev install file.
  * Minimizing rules file.
  * Adding description field in menu file.
  * Updating old category in desktop file.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Mon, 10 Aug 2009 23:15:44 +0200

gnunet-gtk (0.8.0c-1) unstable; urgency=low

  * Using correct rfc-2822 date formats in changelog.
  * Merging upstream version 0.8.0c.
  * Upgrading package to standards 3.8.1.
  * Upgrading gnunet depends to 0.8.0c.
  * Updating section of the debug package.
  * Updating year in copyright file.
  * Tidy rules file.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Mon, 11 May 2009 20:36:49 +0200

gnunet-gtk (0.8.0b-4) unstable; urgency=low

  * Using patch-stamp rather than patch in rules file.
  * Replacing obsolete dh_clean -k with dh_prep.
  * Removing Arnaud from uploaders, he is MIA (Closes: #513164).
  * Updating year in copyright file.
  * Using quilt rather than dpatch.
  * Updating rules to current state of the art.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Thu, 05 Feb 2009 00:36:00 +0100

gnunet-gtk (0.8.0b-3) unstable; urgency=low

  * Adding gnunet-server to suggests (Closes: #499496).
  * Adding note about requiring gnunet-server in gnunet-gtk package
    long-description.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Fri, 19 Sep 2008 10:33:00 +0200

gnunet-gtk (0.8.0b-2) unstable; urgency=low

  * Updating vcs fields in control file.
  * Building gnunet-gtk on all architectures again, guile-1.8 on ia64 is back.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Sun, 14 Sep 2008 13:07:00 +0200

gnunet-gtk (0.8.0b-1) unstable; urgency=low

  * Upgrading gnunet depends to 0.8.0b.
  * Merging upstream version 0.8.0b.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Mon, 25 Aug 2008 00:42:00 +0200

gnunet-gtk (0.8.0a-1) unstable; urgency=low

  * Upgrading gnunet depends to 0.8.0a.
  * Merging upstream version 0.8.0a.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Sun, 03 Aug 2008 20:59:00 +0200

gnunet-gtk (0.8.0-1) unstable; urgency=low

  * Updating configure call in rules for gnunet 0.8.0.
  * Adding librsvg depends.
  * Reordering rules file.
  * Excluding ia64 manually from architectures.
  * Removing watch file.
  * Using lintian debhelper to install lintian overrides.
  * Rewriting copyright file in machine-interpretable format.
  * Upgrading package to standards 3.8.0.
  * Adding vcs fields in control file.
  * Bumping versioned libextractor depends to 0.5.20.
  * Bumping versioned gnunet depends to 0.8.0.
  * Switching maintainer and uploaders fields.
  * Upgrading package to debhelper 7.
  * Merging upstream version 0.8.0.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Thu, 19 Jun 2008 15:43:00 +0200

gnunet-gtk (0.8.0~pre1-1) experimental; urgency=low

  * New upstream release.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Tue, 17 Jun 2008 12:01:00 +0100

gnunet-gtk (0.8.0~pre0-1) experimental; urgency=low

  * New upstream release.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Fri, 07 Mar 2008 16:38:00 +0100

gnunet-gtk (0.7.3-1) unstable; urgency=low

  * New upstream release.
  * Adding suggested improvements from upstream (Closes: #457201):
    - Adding libgksu2-dev to build-depends.
    - Building with --enable-libgksu2 --with-libnotify.
    - Fixing icon install call in rules.
    - Using versioned build-depends against gnunet-tools.
  * Adjusting package sections and priorities.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Mon, 24 Dec 2007 17:06:00 +0100

gnunet-gtk (0.7.2c-4) unstable; urgency=medium

  * Some formal cleanups:
    - Updated to new policy.
    - Using versioned depends for libextractor.
    - Comented lintian overrides.
  * Added missing dh_mkshlibs in rules.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Thu, 06 Dec 2007 23:15:00 +0100

gnunet-gtk (0.7.2c-3) unstable; urgency=low

  * Added depends to gnunet-tools (Closes: #447973).

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Wed, 28 Nov 2007 14:01:00 +0100

gnunet-gtk (0.7.2c-2) unstable; urgency=low

  * Updated menu section (Closes: #444908).

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Thu, 11 Oct 2007 06:27:00 +0200

gnunet-gtk (0.7.2c-1) unstable; urgency=low

  * New upstream release.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Tue, 09 Oct 2007 10:44:00 +0200

gnunet-gtk (0.7.2b-3) unstable; urgency=low

  * Adding debug package.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Wed, 26 Sep 2007 10:35:00 +0200

gnunet-gtk (0.7.2b-2) unstable; urgency=low

  * Bumping build-dependency on gnunet.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Fri, 17 Aug 2007 08:24:00 +0200

gnunet-gtk (0.7.2b-1) unstable; urgency=low

  * New upstream release.
  * Building against fixed gnunet (Closes: #432483).
  * Rewritten/edited all packaging files based on current debhelper templates.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Tue, 14 Aug 2007 15:11:00 +0200

gnunet-gtk (0.7.1c-2) unstable; urgency=low

  * Upload to unstable.
  * Rebuild against gnunet 0.7.1c-2.
  * Added build-depends to libnotify-dev.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Tue, 15 May 2007 10:09:10 +0200

gnunet-gtk (0.7.1c-1) experimental; urgency=low

  * New upstream release (Closes: #420539).
  * Added french translation (Closes: #371147).
  * New desktop icon.
  * Package split into gnunet-gtk and gnunet-gtk-dev.

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Sun, 29 Apr 2007 11:03:10 +0200

gnunet-gtk (0.7.0e-2) unstable; urgency=low

  * New email address.

 -- Daniel Baumann <daniel.baumann@progress-technologies.net>  Tue, 04 Jul 2006 15:49:00 +0200

gnunet-gtk (0.7.0e-1) unstable; urgency=low

  * New upstream release.

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Sun, 21 May 2006 11:57:54 +0200

gnunet-gtk (0.7.0d-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Bumped policy version to 3.7.2.

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Sun, 07 May 2006 16:08:06 +0200

gnunet-gtk (0.7.0c-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Incorporate changes from NMU. Thanks Matej Vela for the
    NMU (Closes: #350150).

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Tue, 21 Mar 2006 19:41:45 +0100

gnunet-gtk (0.7.0b-1.1) unstable; urgency=high

  * NMU.
  * Replace hardcoded dependencies with ${shlibs:Depends}.
    Closes: #350150.

 -- Matej Vela <vela@debian.org>  Mon, 20 Feb 2006 09:11:50 +0100

gnunet-gtk (0.7.0b-1) unstable; urgency=low

  * New upstream release.

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Sun, 25 Dec 2005 00:25:32 +0100

gnunet-gtk (0.7.0a-2) unstable; urgency=low

  * Bumped rev number (wrong file upload). (Closes: #341503)

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Thu, 01 Dec 2005 21:56:10 +0100

gnunet-gtk (0.7.0a-1) unstable; urgency=low

  * New Upstream Release.
  * debian/control: Updated dependencies to follow the libstdc++ allocator change.

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Sun, 20 Nov 2005 12:49:30 +0100

gnunet-gtk (0.7.0-2) unstable; urgency=low

  * debian/control: Fixed dependencies (Closes: #332328)

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Thu, 06 Oct 2005 00:12:23 +0200

gnunet-gtk (0.7.0-1) unstable; urgency=low

  * Initial Release. (Closes: #326604)

 -- Arnaud Kyheng <Arnaud.Kyheng@free.fr>  Sun, 04 Sep 2005 18:30:43 +0200
