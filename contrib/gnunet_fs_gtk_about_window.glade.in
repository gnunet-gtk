<?xml version="1.0" encoding="UTF-8"?>
<interface>
  <requires lib="gtk+" version="2.20"/>
  <!-- interface-naming-policy project-wide -->
  <object class="GtkWindow" id="about_window">
    <property name="can_focus">False</property>
    <property name="modal">True</property>
    <property name="destroy_with_parent">True</property>
    <signal name="realize" handler="GNUNET_GTK_about_window_realized" swapped="no"/>
    <signal name="delete-event" handler="GNUNET_GTK_about_window_got_delete_event" swapped="no"/>
    <child>
      <object class="GtkVBox" id="about_main_vbox">
        <property name="visible">True</property>
        <property name="can_focus">False</property>
        <property name="border_width">10</property>
        <child>
          <object class="GtkImage" id="about_logo_image">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="pixbuf">gnunet-logo-color.png</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">0</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="about_name_version_label">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label">@GNUNET_FS_GTK_NAME@ @GNUNET_FS_GTK_VERSION3@</property>
            <attributes>
              <attribute name="weight" value="bold"/>
              <attribute name="size" value="200"/>
            </attributes>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">1</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="about_copyright_label">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label">(C) @GNUNET_FS_GTK_YEARFROM@-@GNUNET_FS_GTK_YEARTO@ The GNUnet Project</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">2</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel" id="about_link_label">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label">&lt;a href="https://gnunet.org/"&gt;GNUnet: GNU's Framework for Secure P2P Networking&lt;/a&gt;</property>
            <property name="use_markup">True</property>
            <property name="wrap">True</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">3</property>
          </packing>
        </child>
        <child>
          <object class="GtkNotebook" id="about_credits_notebook">
            <property name="can_focus">True</property>
            <child>
              <object class="GtkScrolledWindow" id="about_credits_authors_scroller">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="hscrollbar_policy">automatic</property>
                <property name="vscrollbar_policy">automatic</property>
                <child>
                  <object class="GtkTextView" id="about_credits_authors_textview">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <property name="editable">False</property>
                    <property name="wrap_mode">word</property>
                    <property name="cursor_visible">False</property>
                    <property name="buffer">credits_authors_contents</property>
                    <property name="accepts_tab">False</property>
                  </object>
                </child>
              </object>
            </child>
            <child type="tab">
              <object class="GtkLabel" id="about_credits_authors_tab">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Developed by</property>
              </object>
              <packing>
                <property name="tab_fill">False</property>
              </packing>
            </child>
            <child>
              <object class="GtkScrolledWindow" id="about_credits_docs_scroller">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="hscrollbar_policy">automatic</property>
                <property name="vscrollbar_policy">automatic</property>
                <child>
                  <object class="GtkTextView" id="about_credits_docs_textview">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <property name="editable">False</property>
                    <property name="wrap_mode">word</property>
                    <property name="cursor_visible">False</property>
                    <property name="buffer">credits_docs_contents</property>
                    <property name="accepts_tab">False</property>
                  </object>
                </child>
              </object>
              <packing>
                <property name="position">1</property>
              </packing>
            </child>
            <child type="tab">
              <object class="GtkLabel" id="about_credits_docs_tab">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Documented by</property>
              </object>
              <packing>
                <property name="position">1</property>
                <property name="tab_fill">False</property>
              </packing>
            </child>
            <child>
              <object class="GtkScrolledWindow" id="about_credits_trans_scroller">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="hscrollbar_policy">automatic</property>
                <property name="vscrollbar_policy">automatic</property>
                <child>
                  <object class="GtkTextView" id="about_credits_trans_textview">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <property name="editable">False</property>
                    <property name="wrap_mode">word</property>
                    <property name="cursor_visible">False</property>
                    <property name="buffer">credits_trans_contents</property>
                    <property name="accepts_tab">False</property>
                  </object>
                </child>
              </object>
              <packing>
                <property name="position">2</property>
              </packing>
            </child>
            <child type="tab">
              <object class="GtkLabel" id="about_credits_trans_tab">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Translated by</property>
              </object>
              <packing>
                <property name="position">2</property>
                <property name="tab_fill">False</property>
              </packing>
            </child>
            <child>
              <object class="GtkScrolledWindow" id="about_credits_art_scroller">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="hscrollbar_policy">automatic</property>
                <property name="vscrollbar_policy">automatic</property>
                <child>
                  <object class="GtkTextView" id="about_credits_art_textview">
                    <property name="visible">True</property>
                    <property name="can_focus">True</property>
                    <property name="editable">False</property>
                    <property name="wrap_mode">word</property>
                    <property name="cursor_visible">False</property>
                    <property name="buffer">credits_art_contents</property>
                    <property name="accepts_tab">False</property>
                  </object>
                </child>
              </object>
              <packing>
                <property name="position">3</property>
              </packing>
            </child>
            <child type="tab">
              <object class="GtkLabel" id="about_credits_art_tab">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="label" translatable="yes">Artwork by</property>
              </object>
              <packing>
                <property name="position">3</property>
                <property name="tab_fill">False</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="expand">True</property>
            <property name="fill">True</property>
            <property name="position">4</property>
          </packing>
        </child>
        <child>
          <object class="GtkScrolledWindow" id="about_license_scroller">
            <property name="can_focus">True</property>
            <property name="hscrollbar_policy">never</property>
            <property name="shadow_type">out</property>
            <child>
              <object class="GtkTextView" id="about_license_textview">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="editable">False</property>
                <property name="wrap_mode">word</property>
                <property name="cursor_visible">False</property>
                <property name="buffer">license_contents</property>
                <property name="accepts_tab">False</property>
              </object>
            </child>
          </object>
          <packing>
            <property name="expand">True</property>
            <property name="fill">True</property>
            <property name="position">5</property>
          </packing>
        </child>
        <child>
          <object class="GtkHBox" id="about_buttons_hbox">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="border_width">5</property>
            <property name="homogeneous">True</property>
            <child>
              <object class="GtkButton" id="about_credits_button">
                <property name="label" translatable="yes">Credits</property>
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="receives_default">True</property>
                <property name="use_action_appearance">False</property>
                <signal name="clicked" handler="GNUNET_GTK_about_credits_button_clicked" swapped="no"/>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">False</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkButton" id="about_license_button">
                <property name="label" translatable="yes">License</property>
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="receives_default">True</property>
                <property name="use_action_appearance">False</property>
                <signal name="clicked" handler="GNUNET_GTK_about_license_button_clicked" swapped="no"/>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">False</property>
                <property name="position">1</property>
              </packing>
            </child>
            <child>
              <object class="GtkButton" id="about_close_button">
                <property name="label">gtk-close</property>
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="receives_default">True</property>
                <property name="use_action_appearance">False</property>
                <property name="use_stock">True</property>
                <signal name="clicked" handler="GNUNET_GTK_about_close_button_clicked" swapped="no"/>
              </object>
              <packing>
                <property name="expand">True</property>
                <property name="fill">False</property>
                <property name="pack_type">end</property>
                <property name="position">2</property>
              </packing>
            </child>
            <child>
              <placeholder/>
            </child>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">False</property>
            <property name="pack_type">end</property>
            <property name="position">6</property>
          </packing>
        </child>
      </object>
    </child>
  </object>
  <object class="GtkTextBuffer" id="credits_art_contents">
    <property name="text">Jakub 'jimmac' Steiner &lt;jimmac@ximian.org&gt;
Christian Muellner &lt;chris@flop.de&gt;
Alex Jones &lt;alexrjones@ntlworld.com&gt;
Nicklas Larsson &lt;whybill@gmail.com&gt;
</property>
  </object>
  <object class="GtkTextBuffer" id="credits_authors_contents">
    <property name="text">Juergen Appel &lt;jappel@linux01.gwdg.de&gt;
Krista Grothoff &lt;krista@grothoff.org&gt;
James Blackwell &lt;jblack@linuxguru.net&gt;
Ludovic Courtes &lt;ludo@chbouib.org&gt;
Nils Durner &lt;durner@gnunet.org&gt;
Renaldo Ferreira &lt;rf@cs.purdue.edu&gt;
Christian Grothoff &lt;christian@grothoff.org&gt;
Eric Haumant
Tzvetan Horozov &lt;horozov@motorola.com&gt;
Gerd Knorr &lt;kraxel@bytesex.org&gt;
Werner Koch &lt;libgcrypt@g10code.com&gt;
Uli Luckas &lt;luckas@musoft.de&gt;
Blake Matheny
Glenn McGrath
Hendrik Pagenhardt &lt;Hendrik.Pagenhardt@gmx.net&gt;
Ioana Patrascu &lt;ioanapatrascu@yahoo.com&gt;
Marko Raeihae
Paul Ruth &lt;ruth@cs.purdue.edu&gt;
Risto Saarelma
Antti Salonen
Tiberius Stef &lt;tstef@cs.purdue.edu&gt;
Tuomas Toivonen
Tomi Tukiainen
Kevin Vandersloot &lt;kfv101@psu.edu&gt;
Simo Viitanen
Larry Waldo
Igor Wronsky &lt;iwronsky@users.sourceforge.net&gt;
&lt;january@hushmail.com&gt;</property>
  </object>
  <object class="GtkTextBuffer" id="credits_docs_contents">
    <property name="text">Christian Grothoff &lt;christian@grothoff.org&gt;
Anders Carlsson &lt;andersca@gnu.org&gt;
Nils Durner &lt;durner@gnunet.org&gt;
Nicklas Larsson &lt;whybill@gmail.com&gt;
Milan Bouchet-Valat &lt;nalimilan@club.fr&gt;
Igor Wronsky &lt;iwronsky@users.sourceforge.net&gt;
</property>
  </object>
  <object class="GtkTextBuffer" id="credits_trans_contents">
    <property name="text">Di Ma
Jens Palsberg &lt;palsberg@cs.ucla.edu&gt;
Christian Grothoff &lt;christian@grothoff.org&gt;
Nils Durner &lt;durner@gnunet.org&gt;
Mathieu &lt;mollo@bghflt.org&gt;
Eric Haumant
milan@skoid.org
Hiroshi Yamauchi &lt;yamauchi@cs.purdue.edu&gt;
Adam Welc &lt;welc@cs.purdue.edu&gt;
Bogdan Carbunar &lt;carbunar@cs.purdue.edu&gt;
Steven Michael Murphy &lt;murf@e-tools.com&gt;
Phan Vinh Thinh &lt;teppi82@gmail.com&gt;
Daniel Nylander &lt;po@danielnylander.se&gt;
</property>
  </object>
  <object class="GtkTextBuffer" id="license_contents">
    <property name="text" translatable="yes">License should be loaded here at runtime from the license file (no need to copy the whole GPL in here...).</property>
  </object>
</interface>
