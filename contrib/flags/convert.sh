#!/bin/sh

for n in *.svg
do
  echo $n
  m=`echo $n | sed -e "s/\.svg//"`

  rsvg-convert $n > $m.png
  convert $m.png -resize 100x15 -strip $m.png
done

