
 This is a package of public domain country flags in SVG format.

   The files are named after ISO 3166-1 Alpha-2 (country) codes.

     Each region not having an official flag of its own
     is presented as symbolic link to the flag officially
     used in that region.

   The images were gathered in 2005 from the "Open Clip Art Library"
   and Sodipodi Clipart.

   In 2007 the broken flag of Armenia was replaced with a new one
   from Wikimedia Commons

The plan is to eventually use them in gnunet-gtk for language/country
related display (i.e. list of connected peers, language of text, etc.).


 Toni Ruottu <http://www.cs.helsinki.fi/u/twruottu/>
