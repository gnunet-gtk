#!/bin/sh
# This shell script can be used to configure the "gnunet://" URL handler for Gnome
gconftool-2 -t string -s /desktop/gnome/url-handlers/gnunet/command "gnunet-uri \"%s\""
gconftool-2 -t bool -s /desktop/gnome/url-handlers/gnunet/enabled true
gconftool-2 -t bool -s /desktop/gnome/url-handlers/gnunet/needs_terminal false 

# Uninstall using
# gconftool-2 -u /desktop/gnome/url-handlers/gnunet --recursive-unset

