/*
     This file is part of GNUnet
     Copyright (C) 2005, 2006, 2010, 2011 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/lib/about.c
 * @brief code to display an about dialog
 * @author Christian Grothoff
 * @author Igor Wronsky
 */
#include "gnunet_gtk.h"

/**
 * Context for an about dialog.
 */
struct AboutDialogContext
{
  /**
   * Gtk builder for access to the widgets.
   */
  GtkBuilder *builder;

  /**
   * The main window of the dialog.
   */
  GtkWidget *ad;

  /**
   * Credits notebook.
   */
  GtkWidget *about_credits_notebook;

  /**
   * License notebook.
   */
  GtkWidget *about_license_scroller;

  /**
   * Buffer with the license text.
   */
  GtkTextBuffer *license_contents;
};


/**
 * Destroy all resources associated with the about dialog.
 *
 * @param ctx the dialog's context
 */
static void
destroy_about_dialog (struct AboutDialogContext *ctx)
{
  gtk_widget_destroy (ctx->ad);
  g_object_unref (G_OBJECT (ctx->builder));
  GNUNET_free (ctx);
}


/**
 * Helper function that makes one and only one of the
 * three areas of the dialog visible.
 *
 * @param ctx the dialog context
 * @param name name of the area to make visible, NULL for default
 */
static void
about_window_show_exclusively (struct AboutDialogContext *ctx,
                               const gchar *name)
{
  if (NULL == name)
  {
    gtk_widget_hide (ctx->about_credits_notebook);
    gtk_widget_hide (ctx->about_license_scroller);
  }
  else if (strcmp ("about_credits_notebook", name) == 0)
  {
    gtk_widget_show (ctx->about_credits_notebook);
    gtk_widget_hide (ctx->about_license_scroller);
  }
  else if (strcmp ("about_license_scroller", name) == 0)
  {
    gtk_widget_show (ctx->about_license_scroller);
    gtk_widget_hide (ctx->about_credits_notebook);
  }
}


/**
 * The about window has been realized; load the license file
 * from our resource directory.
 *
 * @param widget unused
 * @param ctx the dialog context
 */
G_MODULE_EXPORT void
GNUNET_GTK_about_window_realized (GtkWidget *widget,
                                  struct AboutDialogContext *ctx)
{
  gchar *license = NULL;
  const char *path;
  char *license_path;

  path = GNUNET_OS_installation_get_path (GNUNET_GTK_project_data (),
                                          GNUNET_OS_IPK_DOCDIR);
  if (path != NULL)
    GNUNET_asprintf (&license_path, "%s%s", path, "COPYING");
  else
    license_path = GNUNET_strdup ("COPYING");
  if (g_file_get_contents (license_path, &license, NULL, NULL) &&
      (license != NULL))
  {
    gtk_text_buffer_set_text (ctx->license_contents, license, -1);
    g_free (license);
  }
  GNUNET_free (license_path);
}


/**
 * The user clicked the "close" button, close the dialog.
 *
 * @param widget the button that was clicked
 * @param ctx our dialog context
 */
G_MODULE_EXPORT void
GNUNET_GTK_about_close_button_clicked (GtkButton *widget,
                                       struct AboutDialogContext *ctx)
{
  destroy_about_dialog (ctx);
}


/**
 * The user pushed the "delete" button, close the dialog.
 *
 * @param widget the widget that received the button
 * @param event the button event
 * @param ctx our dialog context
 */
G_MODULE_EXPORT gboolean
GNUNET_GTK_about_window_got_delete_event (GtkWidget *widget,
                                          GdkEvent *event,
                                          struct AboutDialogContext *ctx)
{
  destroy_about_dialog (ctx);
  return FALSE;
}


/**
 * The user clicked the "credits" button, display the credits.
 *
 * @param widget the button that was clicked
 * @param ctx our dialog context
 */
G_MODULE_EXPORT void
GNUNET_GTK_about_credits_button_clicked (GtkButton *widget,
                                         struct AboutDialogContext *ctx)
{
  about_window_show_exclusively (ctx, "about_credits_notebook");
}


/**
 * The user clicked the "license" button, display the license.
 *
 * @param widget the button that was clicked
 * @param ctx our dialog context
 */
G_MODULE_EXPORT void
GNUNET_GTK_about_license_button_clicked (GtkButton *widget,
                                         struct AboutDialogContext *ctx)
{
  about_window_show_exclusively (ctx, "about_license_scroller");
}


void
GNUNET_GTK_display_about (
  const struct GNUNET_OS_ProjectData *pd,
  const char *dialogfile)
{
  struct AboutDialogContext *ctx;

  ctx = GNUNET_new (struct AboutDialogContext);
  ctx->builder = GNUNET_GTK_get_new_builder (pd,
                                             dialogfile,
                                             ctx);
  if (NULL == ctx->builder)
  {
    GNUNET_free (ctx);
    return;
  }
  ctx->about_credits_notebook = GTK_WIDGET (
    gtk_builder_get_object (ctx->builder,
                            "about_credits_notebook"));
  ctx->about_license_scroller = GTK_WIDGET (
    gtk_builder_get_object (ctx->builder,
                            "about_license_scroller"));
  ctx->ad = GTK_WIDGET (gtk_builder_get_object (ctx->builder,
                                                "about_window"));
  ctx->license_contents =
    GTK_TEXT_BUFFER (gtk_builder_get_object (ctx->builder,
                                             "license_contents"));
  gtk_widget_show (ctx->ad);
}


/* end of about.c */
