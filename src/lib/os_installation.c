/*
     This file is part of GNUnet.
     Copyright (C) 2016 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/lib/os_installation.c
 * @brief initialize GNUNET_OS for gnunet-gtk
 * @author Christian Grothoff
 */
#include "gnunet_gtk_config.h"
#include "gnunet_gtk.h"

/**
 * Default project data used for installation path detection
 * for gnunet-gtk.
 */
static const struct GNUNET_OS_ProjectData gtk_pd = {
  .libname = "libgnunetgtk",
  .project_dirname = "gnunet-gtk",
  .binary_name = "gnunet-fs-gtk",
  .env_varname = "GNUNET_GTK_PREFIX",
  .env_varname_alt = "GNUNET_PREFIX",
  .base_config_varname = "GNUNET_BASE_CONFIG",
  .bug_email = "gnunet-developers@gnu.org",
  .homepage = "http://www.gnu.org/s/gnunet/",
  .config_file = "gnunet.conf",
  .user_config_file = "~/.config/gnunet.conf",
  .gettext_domain = "gnunet-gtk",
  .version = PACKAGE_VERSION,
  .is_gnu = 1
};


/**
 * Return default project data used by gnunet-gtk.
 */
const struct GNUNET_OS_ProjectData *
GNUNET_GTK_project_data (void)
{
  return &gtk_pd;
}


/* end of os_installation.c */
