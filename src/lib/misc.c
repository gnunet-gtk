/*
     This file is part of GNUnet.
     Copyright (C) 2011, 2013 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/lib/misc.c
 * @brief misc functions
 * @author Christian Grothoff
 */
#include "gnunet_gtk.h"


/**
 * Initialize the 'expiration_year_adjustment' of the given
 * builder to have a lower range of current-year+1 and a
 * default of current-year+2.
 *
 * @param builder builder object for which we should manipulate
 * the adjustment
 */
void
GNUNET_GTK_setup_expiration_year_adjustment (GtkBuilder *builder)
{
  GtkAdjustment *aj;
  unsigned int year;

  year = GNUNET_TIME_get_current_year ();
  aj = GTK_ADJUSTMENT (
    gtk_builder_get_object (builder, "expiration_year_adjustment"));
  gtk_adjustment_set_value (aj, year + 2);
  gtk_adjustment_set_lower (aj, year + 1);
}


/**
 * Convert the year from the spin button to an expiration
 * time (on midnight, January 1st of that year).
 *
 * @param spin button with an expiration year
 * @return expiration time in the usual GNUnet format
 */
struct GNUNET_TIME_Absolute
GNUNET_GTK_get_expiration_time (GtkSpinButton *spin)
{
  struct GNUNET_TIME_Absolute ret;
  int year;

  year = gtk_spin_button_get_value_as_int (spin);
  GNUNET_assert (year >= 0);
  ret = GNUNET_TIME_year_to_time ((unsigned int) year);
  GNUNET_break (GNUNET_TIME_absolute_get ().abs_value_us < ret.abs_value_us);
  return ret;
}


/**
 * Obtain the numeric anonymity level selected by a GtkComboBox.
 *
 * @param builder builder for looking up widgets
 * @param combo_name name of the GtkComboBox with the anonymity selection
 * @param p_level where to store the anonymity level
 * @return TRUE on success, FALSE on failure
 */
gboolean
GNUNET_GTK_get_selected_anonymity_level (GtkBuilder *builder,
                                         gchar *combo_name,
                                         guint *p_level)
{
  GtkComboBox *combo;

  combo = GTK_COMBO_BOX (gtk_builder_get_object (builder, combo_name));
  if (! combo)
    return FALSE;
  return GNUNET_GTK_get_selected_anonymity_combo_level (combo, p_level);
}


/**
 * Obtain the numeric anonymity level selected by a GtkComboBox.
 *
 * @param combo the GtkComboBox with the anonymity selection
 * @param p_level where to store the anonymity level
 * @return TRUE on success, FALSE on failure
 */
gboolean
GNUNET_GTK_get_selected_anonymity_combo_level (GtkComboBox *combo,
                                               guint *p_level)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  guint level;

  if (! gtk_combo_box_get_active_iter (combo, &iter))
    return FALSE;
  model = gtk_combo_box_get_model (combo);
  if (! model)
    return FALSE;
  gtk_tree_model_get (model,
                      &iter,
                      GNUNET_GTK_ANONYMITY_LEVEL_MC_LEVEL,
                      &level,
                      -1);
  if (p_level)
    *p_level = level;
  return TRUE;
}


/* end of misc.c */
