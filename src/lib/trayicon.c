/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2011, 2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/lib/trayicon.c
 * @brief trayicon support
 * @author Christian Grothoff
 */
#include "gnunet_gtk_config.h"
#include "gnunet_gtk.h"


/**
 * Our tray icon.
 */
static GtkStatusIcon *tray_icon;

/**
 * The main window.
 */
static GtkWindow *main_window;


/**
 * We got a click on our tray icon. Toggle visibility of the main
 * window.
 *
 * @param status_icon widget that received the click
 * @param user_data unused
 */
static void
tray_icon_on_click (GtkStatusIcon *status_icon, gpointer user_data)
{
  gboolean tlf;

  g_object_get (main_window, "visible", &tlf, NULL);
  if (tlf)
  {
    gtk_widget_hide (GTK_WIDGET (main_window));
  }
  else
  {
    gtk_widget_show (GTK_WIDGET (main_window));
    gtk_window_deiconify (main_window);
    gtk_widget_grab_focus (GTK_WIDGET (main_window));
  }
}


/**
 * Function called from trayicon "quit" context menu
 * to trigger shutdown.
 *
 * @param menuitem quit item
 * @param user_data the main loop context
 */
void
GNUNET_GTK_tray_icon_quit (GtkMenuItem *menuitem, gpointer user_data)
{
  /* struct GNUNET_GTK_MainLoop *ml = user_data; */
  GNUNET_SCHEDULER_shutdown ();
}


/**
 * We got a right-click on the tray icon. Display the context
 * menu (which should have a 'quit' button).
 *
 * @param widget widget that received the click
 * @param event the event details (check for right click)
 * @param user_data unused
 */
static int
tray_icon_on_menu (GtkWidget *widget,
                   GdkEvent *event,
                   gpointer user_data)
{
  struct GNUNET_GTK_MainLoop *ml = user_data;
  GtkMenu *tray_menu;
  GdkEventButton *event_button;
  GtkBuilder *builder;

  if (event->type == GDK_BUTTON_PRESS)
  {
    event_button = (GdkEventButton *) event;
    if (event_button->button == 3)
    {
      builder =
        GNUNET_GTK_get_new_builder ("gnunet_gtk_status_bar_menu.glade", ml);
      tray_menu = GTK_MENU (
        gtk_builder_get_object (builder,
                                "GNUNET_GTK_status_bar_popup_menu"));
      g_object_ref (tray_menu);
      gtk_menu_popup (tray_menu,
                      NULL,
                      NULL,
                      NULL,
                      NULL,
                      event_button->button,
                      event_button->time);
      g_object_unref (builder);
    }
  }
  return FALSE;
}


/**
 * Create our tray icon.
 *
 * @param ml main loop
 * @param main handle to the main window (show or hide)
 * @param icon_name name of the tray icon file
 * @param tooltip tooltip for the tray icon
 */
void
GNUNET_GTK_tray_icon_create (struct GNUNET_GTK_MainLoop *ml,
                             GtkWindow *main,
                             const char *icon_name,
                             const char *tooltip)
{
  if (NULL != tray_icon)
  {
    GNUNET_break (0);
    return;
  }
  main_window = main;
  tray_icon = gtk_status_icon_new ();
  g_signal_connect (G_OBJECT (tray_icon),
                    "activate",
                    G_CALLBACK (tray_icon_on_click),
                    ml);
  g_signal_connect (G_OBJECT (tray_icon),
                    "button_press_event",
                    G_CALLBACK (tray_icon_on_menu),
                    ml);
  gtk_status_icon_set_from_icon_name (tray_icon, icon_name);
  gtk_status_icon_set_tooltip_text (tray_icon, tooltip);
  gtk_status_icon_set_visible (tray_icon, TRUE);
}


/**
 * Change our tray icon.
 *
 * @param icon_name name of the tray icon file
 * @param tooltip tooltip for the tray icon
 */
void
GNUNET_GTK_tray_icon_change (const char *icon_name,
                             const char *tooltip)
{
  if (NULL == tray_icon)
    return;
  gtk_status_icon_set_from_icon_name (tray_icon,
                                      icon_name);
  gtk_status_icon_set_tooltip_text (tray_icon,
                                    tooltip);
  gtk_status_icon_set_visible (tray_icon,
                               TRUE);
}


/**
 * Destroy the tray icon.
 */
void
GNUNET_GTK_tray_icon_destroy ()
{
  if (NULL == tray_icon)
    return;
  g_object_unref (G_OBJECT (tray_icon));
  tray_icon = NULL;
  main_window = NULL;
}


/* end of trayicon.c */
