/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2024 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/lib/glade.c
 * @brief code for integration with glade
 * @author Christian Grothoff
 */
#include "gnunet_gtk_config.h"
#include "gnunet_gtk.h"


void
GNUNET_GTK_set_icon_search_path (const struct GNUNET_OS_ProjectData *pd)
{
  char *buf;

  buf = GNUNET_OS_installation_get_path (pd,
                                         GNUNET_OS_IPK_ICONDIR);
  gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (),
                                     buf);
  GNUNET_free (buf);
}


const char *
GNUNET_GTK_get_data_dir (const struct GNUNET_OS_ProjectData *pd)
{
  static char *dd;

  if (NULL == dd)
    dd = GNUNET_OS_installation_get_path (pd,
                                          GNUNET_OS_IPK_DATADIR);
  return dd;
}


gboolean
GNUNET_GTK_get_tree_string (GtkTreeView *treeview,
                            GtkTreePath *treepath,
                            guint column,
                            gchar **value)
{
  gboolean ok;
  GtkTreeModel *model;
  GtkTreeIter iter;

  model = gtk_tree_view_get_model (treeview);
  if (! model)
    return FALSE;
  ok = gtk_tree_model_get_iter (model, &iter, treepath);
  if (! ok)
    return FALSE;

  *value = NULL;
  gtk_tree_model_get (model, &iter, column, value, -1);
  if (NULL == *value)
    return FALSE;
  return TRUE;
}


GtkBuilder *
GNUNET_GTK_get_new_builder2 (const struct GNUNET_OS_ProjectData *pd,
                             const char *filename,
                             void *user_data,
                             GtkBuilderConnectFunc cb)
{
  char *glade_path;
  GtkBuilder *ret;
  GError *error;

  ret = gtk_builder_new ();
  GNUNET_asprintf (&glade_path,
                   "%s%s",
                   GNUNET_GTK_get_data_dir (pd),
                   filename);
  error = NULL;
  if (0 == gtk_builder_add_from_file (ret,
                                      glade_path,
                                      &error))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                _ ("Failed to load `%s': %s\n"),
                glade_path,
                error->message);
    g_error_free (error);
    GNUNET_free (glade_path);
    return NULL;
  }
  if (NULL == user_data)
    user_data = ret;
  if (NULL != cb)
    gtk_builder_connect_signals_full (ret,
                                      cb, user_data);
  else
    gtk_builder_connect_signals (ret,
                                 user_data);
  GNUNET_free (glade_path);
  return ret;
}


void
GNUNET_FS_GTK_remove_treestore_subtree (GtkTreeStore *ts,
                                        GtkTreeIter *root)
{
  GtkTreeIter child;

  while (gtk_tree_model_iter_children (GTK_TREE_MODEL (ts),
                                       &child,
                                       root))
    GNUNET_FS_GTK_remove_treestore_subtree (ts,
                                            &child);
  gtk_tree_store_remove (ts,
                         root);
}


/* end of glade.c */
