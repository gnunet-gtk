/*
     This file is part of GNUnet.
     Copyright (C) 2010 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-datacache-plugins.c
 * @brief (de)sensitize transport plugin buttons based on plugin availability
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"


/**
 * Test if the given plugin exists and change the sensitivity
 * of the widget accordingly.
 *
 * @param widget widget to update
 * @param name name of the plugin to check
 */
static void
test_plugin (GtkWidget *widget, const char *name)
{
  if (GNUNET_YES ==
      GNUNET_PLUGIN_test (GNUNET_OS_project_data_gnunet (),
                          name))
  {
    gtk_widget_set_sensitive (widget, TRUE);
  }
  else
  {
    gtk_widget_set_sensitive (widget, FALSE);
    gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);
  }
}


void
GNUNET_setup_fs_datacache_sqlite_radiobutton_realize_cb (GtkWidget *widget,
                                                         gpointer user_data)
{
  test_plugin (widget, "libgnunet_plugin_datacache_sqlite");
}


void
GNUNET_setup_fs_datacache_heap_radiobutton_realize_cb (GtkWidget *widget,
                                                       gpointer user_data)
{
  test_plugin (widget, "libgnunet_plugin_datacache_heap");
}


void
GNUNET_setup_fs_datacache_postgres_radiobutton_realize_cb (GtkWidget *widget,
                                                           gpointer user_data)
{
  test_plugin (widget, "libgnunet_plugin_datacache_postgres");
}


/* end of gnunet-setup-transport-plugins.c */
