/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2012 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-namestore-config.c
 * @brief test datastore configuration
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"
#include <gnunet/gnunet_namestore_plugin.h>


/**
 * Test if the configuration works for the given plugin.
 *
 * @param name name of the plugin to check
 * @return #GNUNET_OK on success, #GNUNET_SYSERR if the config is wrong
 */
static int
test_config (const char *name)
{
  void *ret;

  ret = GNUNET_PLUGIN_load (GNUNET_OS_project_data_gnunet (),
                            name,
                            cfg);
  if (NULL == ret)
    return GNUNET_SYSERR;
  GNUNET_PLUGIN_unload (name, ret);
  return GNUNET_OK;
}


static void
show (const char *name)
{
  gtk_widget_show (GTK_WIDGET (GNUNET_SETUP_get_object (name)));
}


static void
hide (const char *name)
{
  gtk_widget_hide (GTK_WIDGET (GNUNET_SETUP_get_object (name)));
}


void
GNUNET_setup_namestore_postgres_invalidate_cb ()
{
  hide ("GNUNET_setup_namestore_postgres_tab_ok_image");
  hide ("GNUNET_setup_namestore_postgres_tab_error_image");
}


void
GNUNET_setup_namestore_postgres_tab_test_button_clicked_cb (GtkWidget *widget,
                                                            gpointer user_data)
{
  if (GNUNET_OK == test_config ("libgnunet_plugin_namestore_postgres"))
  {
    show ("GNUNET_setup_namestore_postgres_tab_ok_image");
    hide ("GNUNET_setup_namestore_postgres_tab_error_image");
  }
  else
  {
    hide ("GNUNET_setup_namestore_postgres_tab_ok_image");
    show ("GNUNET_setup_namestore_postgres_tab_error_image");
  }
}


/* end of gnunet-setup-namestore-config.c */
