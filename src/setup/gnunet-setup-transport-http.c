/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2011 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-transport-http.c
 * @brief support for HTTP configuration
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"
#include "gnunet-setup-transport-test.h"


/**
 * Function called whenever the user wants to test the
 * HTTP configuration.
 */
void
GNUNET_setup_transport_http_test_button_clicked_cb ()
{
  GNUNET_setup_transport_test ("transport-http_server",
                               IPPROTO_TCP,
                               "GNUNET_setup_transport_http_test_success_image",
                               "GNUNET_setup_transport_http_test_fail_image");
}


/* end of gnunet-setup-transport-http.c */
