/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2011 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-transport-test.c
 * @brief support for testing transport configurations
 * @author Christian Grothoff
 */
#ifndef GNUNET_SETUP_TRANSPORT_TEST_H
#define GNUNET_SETUP_TRANSPORT_TEST_H

/**
 * Function called whenever the user wants to test a
 * transport configuration.
 *
 * @param section_name section with the port numbers
 * @param proto IPPROTO_TCP or IPPROTO_UDP
 * @param success_image image to show on success
 * @param failure_image image to show on failure
 */
void
GNUNET_setup_transport_test (const char *section_name,
                             int proto,
                             const char *success_image,
                             const char *failure_image);

#endif
