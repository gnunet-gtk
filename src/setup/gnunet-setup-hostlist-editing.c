/*
     This file is part of GNUnet.
     Copyright (C) 2010 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/setup/gnunet-setup-hostlist-editing.c
 * @brief allow editing of the hostlist
 * @author Christian Grothoff
 */
#include "gnunet-setup.h"

/**
 * Handle editing of text in the GtkListModel.  Changes
 * the new entry to non-editable and creates another
 * empty entry at the end.
 *
 * @param renderer renderer emitting the signal
 * @param path path identifying the edited cell
 * @param new_text text that was added
 * @param user_data not used
 */
void
GNUNET_setup_hostlist_url_cellrenderertext_edited_cb (
  GtkCellRendererText *renderer,
  gchar *path,
  gchar *new_text,
  gpointer user_data)
{
  GtkListStore *ls;
  GtkTreeIter old;
  GtkTreeIter iter;
  gchar *oldtext;

  ls = GTK_LIST_STORE (
    GNUNET_SETUP_get_object ("GNUNET_setup_hostlist_url_liststore"));
  if (NULL == ls)
  {
    GNUNET_break (0);
    return;
  }
  if (! gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL (ls), &old, path))
  {
    GNUNET_break (0);
    return;
  }
  gtk_tree_model_get (GTK_TREE_MODEL (ls),
                      &old,
                      GNUNET_GTK_SETUP_HOSTLIST_URL_MC_URL,
                      &oldtext,
                      -1);
  if (0 == strlen (oldtext))
  {
    gtk_list_store_insert_before (ls, &iter, &old);
    gtk_list_store_set (ls,
                        &iter,
                        GNUNET_GTK_SETUP_HOSTLIST_URL_MC_URL,
                        new_text,
                        GNUNET_GTK_SETUP_HOSTLIST_URL_MC_EDITABLE,
                        FALSE,
                        -1);
  }
  else
  {
    gtk_list_store_set (ls,
                        &old,
                        GNUNET_GTK_SETUP_HOSTLIST_URL_MC_URL,
                        new_text,
                        GNUNET_GTK_SETUP_HOSTLIST_URL_MC_EDITABLE,
                        FALSE,
                        -1);
  }
  g_free (oldtext);
}

/* end of gnunet-setup-hostlist-editing.c */
