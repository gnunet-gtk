/*
     This file is part of GNUnet.
     Copyright (C) 2007, 2011 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/peerinfo/gnunet-peerinfo-gtk-flags.h
 * @brief flag lookup
 * @author Christian Grothoff
 */
#ifndef GNUNET_PEERINFO_GTK_FLAGS_H
#define GNUNET_PEERINFO_GTK_FLAGS_H

#include "gnunet_gtk.h"
#include <ctype.h>
#include <gdk/gdk.h>

/**
 * Lookup the flag image for the given country code
 *
 * @return NULL on error
 */
GdkPixbuf *
GNUNET_PEERINFO_GTK_get_flag (const char *cc);


/**
 * Deallocate all cached flags.
 */
void
GNUNET_PEERINFO_GTK_flags_shutdown (void);


#endif
