/*
     This file is part of GNUnet
     Copyright (C) 2005, 2006, 2010, 2012 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_anonymity-widgets.c
 * @author Christian Grothoff
 * @brief operations to manage user's anonymity level selections
 */
#include "gnunet-fs-gtk.h"
#include "gnunet-fs-gtk_common.h"
#include "gnunet-fs-gtk_anonymity-widgets.h"
#include <gdk/gdk.h>


/**
 * Spin button is changed, update its color.  NOTE: This function will eventually
 * become obsolete as we migrate to the drop-down style of anonymity-level selection.
 *
 * @param w the spin button that changed
 * @param data the builder's closure, unused
 */
void
GNUNET_GTK_anonymity_spin_button_value_changed_cb (GtkWidget *w, gpointer data)
{
#ifdef GdkRBGA
  GtkSpinButton *spin;
  gint val;
  GdkRGBA bcolor;
  GdkRGBA fcolor;

  spin = GTK_SPIN_BUTTON (w);
  if (spin == NULL)
  {
    GNUNET_break (0);
    return;
  }
  val = gtk_spin_button_get_value_as_int (spin);
  if (val == 0)
  {
    if ((TRUE == gdk_rgba_parse (&bcolor, "red")) &&
        (TRUE == gdk_rgba_parse (&fcolor, "black")))
    {
      gtk_widget_override_background_color (w, GTK_STATE_NORMAL, &bcolor);
      gtk_widget_override_color (w, GTK_STATE_NORMAL, &fcolor);
    }
    else
    {
      GNUNET_break (0);
    }
  }
  else
  {
    gtk_widget_override_background_color (w, GTK_STATE_NORMAL, NULL);
    gtk_widget_override_color (w, GTK_STATE_NORMAL, NULL);
  }
#endif
}


/**
 * Set the anonymity level displayed by a combo box.
 *
 * @param builder the builder of the combo box
 * @param combo_name name of the combo box
 * @param sel_level desired anonymity level
 * @return TRUE on success, FALSE on failure
 */
gboolean
GNUNET_GTK_select_anonymity_level (GtkBuilder *builder,
                                   gchar *combo_name,
                                   guint sel_level)
{
  GtkComboBox *combo;

  combo = GTK_COMBO_BOX (gtk_builder_get_object (builder, combo_name));
  if (! combo)
    return FALSE;
  return GNUNET_GTK_select_anonymity_combo_level (combo, sel_level);
}


/**
 * Set the anonymity level displayed by a combo box.
 *
 * @param combo the combo box
 * @param sel_level desired anonymity level
 * @return TRUE on success, FALSE on failure
 */
gboolean
GNUNET_GTK_select_anonymity_combo_level (GtkComboBox *combo, guint sel_level)
{
  GtkTreeIter iter;
  GtkTreeModel *model;
  guint level;

  model = gtk_combo_box_get_model (combo);
  if (! model)
    return FALSE;
  if (! gtk_tree_model_get_iter_first (model, &iter))
    return FALSE;
  do
  {
    gtk_tree_model_get (model,
                        &iter,
                        GNUNET_GTK_ANONYMITY_LEVEL_MC_LEVEL,
                        &level,
                        -1);
    if (level == sel_level)
    {
      gtk_combo_box_set_active_iter (combo, &iter);
      return TRUE;
    }
  } while (gtk_tree_model_iter_next (model, &iter));
  return FALSE;
}


/* end of gnunet-fs-gtk-anonymtiy_spin_buttons.c */
