This file describes the various things we should test before a release:

gnunet-identity-gtk:
====================
- create namespace X
- advertise namespace X under keyword "ns"

gnunet-namespace:
=================
- set pseudonym "pseu" for ego X

gnunet-fs-gtk:
==============
* about window
* publish: 
  - INSERT image file (under keyword 'f')
    - edit keywords    
    - check preview extraction by LE
    - edit preview
    - also advertise in namespace X under identifier 't' with update 'u'
    - also advertise in namespace X under identifier 'u'
  - INDEX directory (under keyword 'd'); 
    - edit hierarchy (add file, remove file, add keywords)
    - persistence: close gnunet-fs-gtk during publishing, then restart, check resume
* search:
  - check search by passing KSK/SKS URIs on command line
  - file under f 
    - check meta data (toggle visibility)
    - check preview (toggle visibility)
    - test download
  - search namespace X under 'ns'
  - search directory under 'd'
    - test individual download
    - test recursive download
  - search X under 't'
    - check update
  - search X under 'u'
  - search with mime type restriction for image file
* download
  - check download by URI on command-line
  - abort download
  - save as...
* directory
  - open directory (.gnd) file
    - check download from that directory
    - check download of embedded (tiny) file in directory
* unindex
  - check list of indexed files for directory
  - unindex
  - search for unindexed file (should not show by extracted keywords)

