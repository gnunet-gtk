/*
     This file is part of GNUnet.
     Copyright (C) 2010 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_download-save-as.h
 * @brief functions for downloading
 * @author Christian Grothoff
 */
#ifndef GNUNET_FS_GTK_DOWNLOAD_SAVE_AS_H
#define GNUNET_FS_GTK_DOWNLOAD_SAVE_AS_H

#include "gnunet-fs-gtk_common.h"
#include "gnunet-fs-gtk_event-handler.h"


/**
 * Actually start the download that is specified by the given download
 * context and its options.  Will add a download entry to the
 * respective tree model and trigger a start of the download using the
 * FS-API.
 *
 * @param de download entry of the download to execute
 */
void
GNUNET_FS_GTK_download_context_start_download (struct DownloadEntry *de);


/**
 * Open the 'save as' dialog for a download.  Calls the 'dc->cb'
 * continutation when the dialog is complete.  Will release the 'dc'
 * resources if the dialog is cancelled.
 * Pressing 'Save' button will initiate the download.
 *
 * @param de download context for the file/directory
 */
void
GNUNET_FS_GTK_open_download_as_dialog (struct DownloadEntry *de);

/**
 * Open the 'save as' dialog for a download.  Calls the 'dc->cb'
 * continutation when the dialog is complete.  Will release the 'dc'
 * resources if the dialog is cancelled.
 * Pressing 'Save' button will change selected directory and
 * file name in download panel, but will not initiate the download.
 *
 * @param de download context for the file/directory
 */
void
GNUNET_FS_GTK_open_change_download_name_dialog (struct DownloadEntry *de);


/**
 * Free resources associated with the given download entry.
 *
 * @param de context to free
 */
void
GNUNET_FS_GTK_free_download_entry (struct DownloadEntry *de);


#endif
