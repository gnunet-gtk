/*
     This file is part of GNUnet
     Copyright (C) 2005, 2006, 2010 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk_publish-edit-dialog.h
 * @author Christian Grothoff
 */
#ifndef GNUNET_FS_GTK_PUBLISH_EDIT_DIALOG_H
#define GNUNET_FS_GTK_PUBLISH_EDIT_DIALOG_H

#include "gnunet-fs-gtk_common.h"
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_fs_service.h>


/**
 * Function called when the edit publish dialog has been closed.
 *
 * @param cls closure
 * @param ret GTK_RESPONSE_OK if the dialog was closed with "OK"
 * @param root namespace root, NULL for file publishing
 */
typedef void (*GNUNET_FS_GTK_EditPublishDialogCallback) (gpointer cls,
                                                         int ret,
                                                         const char *root);


/**
 * Open the dialog to edit file information data.
 *
 * @param parent parent window of the dialog
 * @param fip information about the file information that is to be edited
 * @param is_namespace_edit GNUNET_YES if we are editing a namespace advertisement;
 *        this means that keywords are required and that a "root" can be
 *        entered
 * @param cb function to call when the dialog is closed
 * @param cb_cls closure for 'cb'
 */
void
GNUNET_FS_GTK_edit_publish_dialog (GtkWindow *parent,
                                   struct GNUNET_FS_FileInformation *fip,
                                   int is_namespace_edit,
                                   GNUNET_FS_GTK_EditPublishDialogCallback cb,
                                   gpointer cls);

#endif
/* end of gnunet-fs-gtk-edit_publish_dialog.h */
