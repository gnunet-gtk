/*
     This file is part of GNUnet.
     Copyright (C) 2010 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/fs/gnunet-fs-gtk.h
 * @brief Globals for gnunet-fs-gtk
 * @author Christian Grothoff
 */
#ifndef GNUNET_FS_GTK_H
#define GNUNET_FS_GTK_H

#include "gnunet_gtk.h"
#include <gnunet/gnunet_fs_service.h>
#include <gnunet/gnunet_gns_service.h>
#include <gnunet/gnunet_identity_service.h>
#include <gnunet/gnunet_namestore_service.h>
#if HAVE_EXTRACTOR_H
#include <extractor.h>
#endif
#include <sys/stat.h>

/**
 * Context for a GNS lookup for starting a search.
 */
struct SearchLookup
{

  /**
   * This is a DLL.
   */
  struct SearchLookup *prev;

  /**
   * This is a DLL.
   */
  struct SearchLookup *next;

  /**
   * Our active request with GNS.
   */
  struct GNUNET_GNS_LookupRequest *gns;

  /**
   * Keywords to use.
   */
  gchar *keywords;

  /**
   * Task to trigger timeout.
   */
  struct GNUNET_SCHEDULER_Task *timeout_task;

  /**
   * Desired anonymity level.
   */
  guint anonymity_level;
};


/**
 * Context we keep for a pseudonym lookup on 'save'.
 */
struct PseuLookupContext
{

  /**
   * Kept in a DLL.
   */
  struct PseuLookupContext *next;

  /**
   * Kept in a DLL.
   */
  struct PseuLookupContext *prev;

  /**
   * Main window context.
   */
  struct GNUNET_GTK_MainWindowContext *main_ctx;

  /**
   * Lookup request with GNS.
   */
  struct GNUNET_GNS_LookupRequest *lr;

  /**
   * Builder for the progress dialog that is displayed.
   */
  GtkBuilder *progress_dialog_builder;

  /**
   * The progress dialog itself.
   */
  GtkWidget *progress_dialog;

  /**
   * Builder for the nickname dialog that is displayed.
   */
  GtkBuilder *nick_dialog_builder;

  /**
   * The nick dialog itself.
   */
  GtkWidget *nick_dialog;

  /**
   * Handle to the namestore.
   */
  struct GNUNET_NAMESTORE_Handle *namestore;

  /**
   * Handle to our namestore request.
   */
  struct GNUNET_NAMESTORE_QueueEntry *qe;

  /**
   * Public key of the namespace we are trying to save.
   */
  struct GNUNET_CRYPTO_PublicKey pkey;

  /**
   * Nickname we're saving under.
   */
  char *nick;
};


/**
 * Abort the given search lookup.
 *
 * @param sl lookup to abort.
 */
void
abort_search_lookup (struct SearchLookup *sl);


/**
 * Abort the given PSEU lookup.
 *
 * @param lctx lookup to abort.
 */
void
abort_pseu_lookup (struct PseuLookupContext *lctx);


/**
 * Context for the main window.
 */
struct GNUNET_GTK_MainWindowContext
{
  /**
   * Builder that loaded the main window.
   */
  GtkBuilder *builder;

  const struct GNUNET_CONFIGURATION_Handle *gnunet_cfg;
  const struct GNUNET_CONFIGURATION_Handle *gnunet_gtk_cfg;

  GtkTreeStore *search_ns_treestore;
  GtkTreeView *ns_selector_treeview;
  GtkWidget *ns_selector_window;
  GtkToggleButton *ns_dropdown_button;
  GtkLabel *search_ns_label;

  GtkEntry *search_entry;

  GtkComboBox *anonymity_combo;
  GtkListStore *anonymity_level_liststore;

  GtkImage *preview_image;
  GtkListStore *md_liststore;
  GtkTreeView *md_treeview;

  GtkWidget *main_window;

  GtkTreeRowReference *ns_selector_pushed_row;
  GtkTreeRowReference *selected_ns_row;
  struct GNUNET_FS_Pseudonym_DiscoveryHandle *ns_discovery_handle;
  GtkWindow *ns_manager;
  gulong ns_manager_delete_handler_id;

  GtkWindow *ns_organizer;
  gulong ns_organizer_delete_handler_id;

  GtkFileChooser *download_location_chooser;
  GtkEntry *download_name_entry;
  GtkComboBox *download_anonymity_combo;
  GtkCheckButton *download_recursive_checkbutton;
  GtkButton *download_download_button;
  GtkBox *download_panel;

  GtkNotebook *notebook;

  GtkImage *connection_indicator;

  /**
   * Handle to the GNS service.
   */
  struct GNUNET_GNS_Handle *gns;

  /**
   * Handle to a zone monitor to update the namespace's drop-down list store.
   * Monitors the @e sks_zone. Can be NULL.
   */
  struct GNUNET_NAMESTORE_ZoneMonitor *zm;

  /**
   * Head of sl DLL.
   */
  struct SearchLookup *sl_head;

  /**
   * Tail of sl DLL.
   */
  struct SearchLookup *sl_tail;

  /**
   * Head of lctx DLL.
   */
  struct PseuLookupContext *lctx_head;

  /**
   * Tail of lctx DLL.
   */
  struct PseuLookupContext *lctx_tail;

  /**
   * Handle to identity service.
   */
  struct GNUNET_IDENTITY_Handle *identity;

  /**
   * Operation we use to determine namespace resolution domain.
   */
  struct GNUNET_IDENTITY_EgoLookup *id_op;

  /**
   * Our zone for SKS operations.  Can be NULL.
   */
  struct GNUNET_IDENTITY_Ego *sks_zone;
};


/**
 * Columns in the search namespace model.
 */
enum GNUNET_GTK_FS_MAIN_WINDOW_SearchNamespaceModelColumns
{
  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_NAMESPACE_MC_NAME = 0,

  /**
   * A gpointer.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_NAMESPACE_MC_KEY = 1,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_NAMESPACE_MC_ROOT = 2,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_NAMESPACE_MC_TOOLTIP = 3,
};


/**
 * Columns in the meta data model.
 */
enum GNUNET_GTK_FS_MAIN_WINDOW_MetaDataModelColumns
{
  /**
   * A guint.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_META_DATA_MC_META_TYPE = 0,

  /**
   * A guint.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_META_DATA_MC_META_FORMAT = 1,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_META_DATA_MC_META_TYPE_STRING = 2,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_META_DATA_MC_META_VALUE = 3,
};


/**
 * Columns in the search mime model.
 */
enum GNUNET_GTK_FS_MAIN_WINDOW_SearchMimeModelColumns
{
  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_MIME_MC_MIME = 0,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_MIME_MC_TYPE = 1,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_MIME_MC_EXTENSION = 2,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_MIME_MC_LOSSYNESS = 3,

  /**
   * A gchararray.
   */
  GNUNET_GTK_FS_MAIN_WINDOW_SEARCH_MIME_MC_FREEDOM = 4,
};

/**
 * Get main context.
 */
struct GNUNET_GTK_MainWindowContext *
GNUNET_FS_GTK_get_main_context (void);


/**
 * Get our configuration.
 *
 * @return configuration handle
 */
const struct GNUNET_CONFIGURATION_Handle *
GNUNET_FS_GTK_get_gnunet_configuration (void);

/**
 * Get our configuration.
 *
 * @return configuration handle
 */
const struct GNUNET_CONFIGURATION_Handle *
GNUNET_FS_GTK_get_gnunet_gtk_configuration (void);


/**
 * Return the list store with anonymity levels.
 *
 * @return the list store
 */
GtkTreeModel *
GNUNET_FS_GTK_get_anonymity_level_list_store (void);


/**
 * Return our handle for file-sharing operations.
 *
 * @return NULL on error
 */
struct GNUNET_FS_Handle *
GNUNET_FS_GTK_get_fs_handle (void);


/**
 * Remember FS handle if we don't have one yet.
 *
 * @param fsh file sharing handle to use
 */
void
GNUNET_FS_GTK_set_fs_handle (struct GNUNET_FS_Handle *fsh);

/**
 * Get an object from the main window.
 *
 * @param name name of the object
 * @return NULL on error
 */
GObject *
GNUNET_FS_GTK_get_main_window_object (const char *name);


/**
 * Get the selected anonymity level.
 *
 * @param builder builder object for the window
 * @param combo_name name of the combobox widget to get anonymity from
 * @param p_level pointer to a guint to receive the level value
 * @return TRUE on success, FALSE on error
 */
gboolean
GNUNET_GTK_get_selected_anonymity_level (GtkBuilder *builder,
                                         gchar *combo_name,
                                         guint *p_level);


/**
 * Get the selected anonymity level.
 *
 * @param combo combo box widget to get anonymity from
 * @param p_level pointer to a guint to receive the level value
 * @return TRUE on success, FALSE on error
 */
gboolean
GNUNET_GTK_get_selected_anonymity_combo_level (GtkComboBox *combo,
                                               guint *p_level);


/**
 * Set the selected anonymity level.
 * For dialogue initialization.
 *
 * @param builder builder object for the window
 * @param combo_name name of the combobox widget to set anonymity in
 * @param sel_level the level value to select
 * @return TRUE on success, FALSE on error (no item with such level)
 */
gboolean
GNUNET_GTK_select_anonymity_level (GtkBuilder *builder,
                                   gchar *combo_name,
                                   guint sel_level);


/**
 * Set the selected anonymity level.
 * For dialogue initialization.
 *
 * @param combo the combobox widget to set anonymity in
 * @param sel_level the level value to select
 * @return TRUE on success, FALSE on error (no item with such level)
 */
gboolean
GNUNET_GTK_select_anonymity_combo_level (GtkComboBox *combo, guint sel_level);


#endif
/* end of gnunet-fs-gtk.h */
