/*
 * This file is part of GNUnet
 * Copyright (C) 2009-2013 GNUnet e.V.
 *
 * GNUnet is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3, or (at your
 * option) any later version.
 *
 * GNUnet is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNUnet; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * @file namestore/plugin_gtk_namestore_vpn.c
 * @brief namestore plugin for editing VPN records
 * @author Christian Grothoff
 */
#include "gnunet_gtk.h"
#include "gnunet_gtk_namestore_plugin.h"


/**
 * The user has edited the VPN record value.  Enable/disable 'save'
 * button depending on the validity of the value.
 *
 * @param entry editing widget
 * @param user_data the plugin environment
 */
static void
GNS_edit_dialog_vpn_peer_entry_changed_cb (GtkEditable *entry,
                                           gpointer user_data)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *edc = user_data;

  edc->check_validity (edc);
}


/**
 * Function that will be called to initialize the builder's
 * widgets from the existing record (if there is one).
 * The `n_value` is the existing value of the record as a string.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param n_value the record as a string
 * @param builder the edit dialog's builder
 */
static void
vpn_load (void *cls, gchar *n_value, GtkBuilder *builder)
{
  char s_peer[53 + 1];
  char s_serv[253 + 1];
  unsigned int proto;

  if (3 != sscanf (n_value, "%u %53s %253s", &proto, s_peer, s_serv))
  {
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                _ ("Unable to parse VPN record string `%s'\n"),
                n_value);
    return;
  }
  gtk_entry_set_text (
    GTK_ENTRY (
      gtk_builder_get_object (builder, "edit_dialog_vpn_identifier_entry")),
    s_serv);
  gtk_entry_set_text (GTK_ENTRY (
                        gtk_builder_get_object (builder,
                                                "edit_dialog_vpn_peer_entry")),
                      s_peer);
  if (IPPROTO_UDP == proto)
    gtk_toggle_button_set_active (
      GTK_TOGGLE_BUTTON (
        gtk_builder_get_object (builder,
                                "edit_dialog_vpn_protocol_udp_radiobutton")),
      TRUE);
  else
    gtk_toggle_button_set_active (
      GTK_TOGGLE_BUTTON (
        gtk_builder_get_object (builder,
                                "edit_dialog_vpn_protocol_tcp_radiobutton")),
      TRUE);
}


/**
 * Function that will be called to retrieve the final value of the
 * record (in string format) once the dialog is being closed.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param builder the edit dialog's builder
 * @return record value as a string, as specified in the dialog
 */
static gchar *
vpn_store (void *cls, GtkBuilder *builder)
{
  GtkEntry *entry;
  const gchar *identifier;
  const gchar *peer;
  unsigned int proto;
  char *result;

  entry = GTK_ENTRY (
    gtk_builder_get_object (builder, "edit_dialog_vpn_identifier_entry"));
  identifier = gtk_entry_get_text (entry);
  entry =
    GTK_ENTRY (gtk_builder_get_object (builder, "edit_dialog_vpn_peer_entry"));
  peer = gtk_entry_get_text (entry);
  proto =
    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (
      gtk_builder_get_object (builder,
                              "edit_dialog_vpn_protocol_tcp_radiobutton")))
      ? IPPROTO_TCP
      : IPPROTO_UDP;
  GNUNET_asprintf (&result,
                   "%u %s %s",
                   proto,
                   (const char *) peer,
                   (const char *) identifier);
  return result;
}


/**
 * Function to call to validate the state of the dialog.  Should
 * return #GNUNET_OK if the information in the dialog is valid, and
 * #GNUNET_SYSERR if some fields contain invalid values.  The
 * function should highlight fields with invalid inputs for the
 * user.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param builder the edit dialog's builder
 * @return #GNUNET_OK if there is a valid record value in the dialog
 */
static int
vpn_validate (void *cls, GtkBuilder *builder)
{
  GtkEditable *entry;
  const gchar *preedit;
  struct GNUNET_CRYPTO_EddsaPublicKey pk;

  entry = GTK_EDITABLE (
    gtk_builder_get_object (builder, "edit_dialog_vpn_peer_entry")),
  preedit = gtk_editable_get_chars (entry, 0, -1);
  if ((NULL == preedit) ||
      (GNUNET_OK !=
       GNUNET_CRYPTO_eddsa_public_key_from_string (preedit,
                                                   strlen (preedit),
                                                   &pk)))
    return GNUNET_SYSERR;
  return GNUNET_OK;
}


/**
 * Entry point for the plugin.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment`
 * @return NULL on error, otherwise the plugin context
 */
void *
libgnunet_plugin_gtk_namestore_vpn_init (void *cls)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *env = cls;
  struct GNUNET_GTK_NAMESTORE_PluginFunctions *plugin;
  static struct GNUNET_GTK_NAMESTORE_Symbol symbols[] =
    {{"GNS_edit_dialog_vpn_peer_entry_changed_cb",
      G_CALLBACK (GNS_edit_dialog_vpn_peer_entry_changed_cb)},
     {NULL, NULL}};

  plugin = GNUNET_new (struct GNUNET_GTK_NAMESTORE_PluginFunctions);
  plugin->cls = env;
  plugin->dialog_glade_filename = "gnunet_namestore_edit_vpn.glade";
  plugin->dialog_widget_name = "edit_vpn_dialog";
  plugin->symbols = symbols;
  plugin->load = &vpn_load;
  plugin->store = &vpn_store;
  plugin->validate = &vpn_validate;
  return plugin;
}


/**
 * Exit point from the plugin.
 *
 * @param cls the plugin context (as returned by "init")
 * @return always NULL
 */
void *
libgnunet_plugin_gtk_namestore_vpn_done (void *cls)
{
  struct GNUNET_GTK_NAMESTORE_PluginFunctions *plugin = cls;

  GNUNET_free (plugin);
  return NULL;
}

/* end of plugin_gtk_namestore_vpn.c */
