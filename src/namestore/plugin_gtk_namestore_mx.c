/*
 * This file is part of GNUnet
 * Copyright (C) 2009-2013 GNUnet e.V.
 *
 * GNUnet is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation; either version 3, or (at your
 * option) any later version.
 *
 * GNUnet is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with GNUnet; see the file COPYING.  If not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * @file namestore/plugin_gtk_namestore_mx.c
 * @brief namestore plugin for editing A records
 * @author Christian Grothoff
 */
#include "gnunet_gtk.h"
#include "gnunet_gtk_namestore_plugin.h"


/**
 * The user has edited the MX record value.  Enable/disable 'save'
 * button depending on the validity of the value.
 *
 * @param entry editing widget
 * @param user_data the plugin environment
 */
static void
GNS_edit_dialog_mx_entry_changed_cb (GtkEditable *entry, gpointer user_data)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *edc = user_data;

  edc->check_validity (edc);
}


/**
 * Function that will be called to initialize the builder's
 * widgets from the existing record (if there is one).
 * The `n_value` is the existing value of the record as a string.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param n_value the record as a string
 * @param builder the edit dialog's builder
 */
static void
mx_load (void *cls, gchar *n_value, GtkBuilder *builder)
{
  uint16_t mx_pref;
  char result[253 + 1];

  if (2 != sscanf (n_value, "%hu,%253s", &mx_pref, result))
  {
    GNUNET_break (0);
    GNUNET_log (GNUNET_ERROR_TYPE_ERROR,
                _ ("Unable to parse MX record `%s'\n"),
                n_value);
    return;
  }
  gtk_entry_set_text (GTK_ENTRY (
                        gtk_builder_get_object (builder,
                                                "edit_dialog_mx_entry")),
                      result);
  gtk_spin_button_set_value (
    GTK_SPIN_BUTTON (
      gtk_builder_get_object (builder, "edit_dialog_mx_distance_spinbutton")),
    mx_pref);
}


/**
 * Function that will be called to retrieve the final value of the
 * record (in string format) once the dialog is being closed.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param builder the edit dialog's builder
 * @return record value as a string, as specified in the dialog
 */
static gchar *
mx_store (void *cls, GtkBuilder *builder)
{
  GtkEntry *entry;
  const gchar *value;
  char *result;
  unsigned int distance;
  gchar *ret;

  entry = GTK_ENTRY (gtk_builder_get_object (builder, "edit_dialog_mx_entry"));
  value = gtk_entry_get_text (entry);
  distance = gtk_spin_button_get_value (GTK_SPIN_BUTTON (
    gtk_builder_get_object (builder, "edit_dialog_mx_distance_spinbutton")));
  GNUNET_asprintf (&result, "%hu,%s", distance, value);
  ret = g_strdup (result);
  GNUNET_free (result);
  return ret;
}


/**
 * Function to call to validate the state of the dialog.  Should
 * return #GNUNET_OK if the information in the dialog is valid, and
 * #GNUNET_SYSERR if some fields contain invalid values.  The
 * function should highlight fields with invalid inputs for the
 * user.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment *`
 * @param builder the edit dialog's builder
 * @return #GNUNET_OK if there is a valid record value in the dialog
 */
static int
mx_validate (void *cls, GtkBuilder *builder)
{
  GtkEditable *entry;
  const gchar *preedit;

  entry =
    GTK_EDITABLE (gtk_builder_get_object (builder, "edit_dialog_mx_entry")),
  preedit = gtk_editable_get_chars (entry, 0, -1);
  if ((NULL == preedit) || (GNUNET_OK != GNUNET_DNSPARSER_check_name (preedit)))
    return GNUNET_SYSERR;
  return GNUNET_OK;
}


/**
 * Entry point for the plugin.
 *
 * @param cls the `struct GNUNET_GTK_NAMESTORE_PluginEnvironment`
 * @return NULL on error, otherwise the plugin context
 */
void *
libgnunet_plugin_gtk_namestore_mx_init (void *cls)
{
  struct GNUNET_GTK_NAMESTORE_PluginEnvironment *env = cls;
  struct GNUNET_GTK_NAMESTORE_PluginFunctions *plugin;
  static struct GNUNET_GTK_NAMESTORE_Symbol symbols[] =
    {{"GNS_edit_dialog_mx_entry_changed_cb",
      G_CALLBACK (GNS_edit_dialog_mx_entry_changed_cb)},
     {NULL, NULL}};

  plugin = GNUNET_new (struct GNUNET_GTK_NAMESTORE_PluginFunctions);
  plugin->cls = env;
  plugin->dialog_glade_filename = "gnunet_namestore_edit_mx.glade";
  plugin->dialog_widget_name = "edit_mx_dialog";
  plugin->symbols = symbols;
  plugin->load = &mx_load;
  plugin->store = &mx_store;
  plugin->validate = &mx_validate;
  return plugin;
}


/**
 * Exit point from the plugin.
 *
 * @param cls the plugin context (as returned by "init")
 * @return always NULL
 */
void *
libgnunet_plugin_gtk_namestore_mx_done (void *cls)
{
  struct GNUNET_GTK_NAMESTORE_PluginFunctions *plugin = cls;

  GNUNET_free (plugin);
  return NULL;
}

/* end of plugin_gtk_namestore_mx.c */
