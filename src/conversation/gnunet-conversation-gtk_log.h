/*
     This file is part of GNUnet
     Copyright (C) 2013-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_log.h
 * @author Christian Grothoff
 */
#ifndef GNUNET_CONVERSATION_GTK_LOG_H
#define GNUNET_CONVERSATION_GTK_LOG_H

#include <stdbool.h>
#include <stdint.h>
#include "gnunet_gtk.h"
#include <gnunet/gnunet_identity_service.h>
#include <gnunet/gnunet_namestore_service.h>
#include <gnunet/gnunet_conversation_service.h>
#include <gnunet/gnunet_speaker_lib.h>
#include <gnunet/gnunet_microphone_lib.h>
#include <gnunet/gnunet_identity_service.h>
#include <gnunet/gnunet_namestore_service.h>
#include <gnunet/gnunet_gnsrecord_lib.h>


/**
 * Update status bar.
 *
 * @param message format string for message to put in statusbar
 * @param ... arguments for the format string
 */
void
GCG_update_status_bar (const gchar *message, ...);


/**
 * Log a message to the log textbuffer.
 *
 * @param message format string for message to be logged
 * @param ... arguments for the format string
 */
void
GCG_log (const char *message, ...);


/**
 * Updates the status icon to the image of the given name.
 *
 * @param icon_name name of the icon to use
 */
void
GCG_set_status_icon (const char *icon_name);


#endif
