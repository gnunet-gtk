/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_contacts.h
 * @brief handle requests from user to import new contacts
 * @author yids
 * @author hark
 * @author Christian Grothoff
 */
#ifndef GNUNET_CONVERSATION_GTK_IMPORTS_H
#define GNUNET_CONVERSATION_GTK_IMPORTS_H


/**
 * Add new contact to the address book.
 *
 * @param name
 * @param address
 */
void
GSC_add_contact (const gchar *name, const gchar *address);


/**
 * Add phone address to namestore.
 *
 * @param label label to use for the phone record
 * @param rd record data to publish
 */
void
GSC_add_phone (const gchar *label, const struct GNUNET_GNSRECORD_Data *rd);


/**
 * Remove previously added phone address from namestore.
 */
void
GSC_remove_phone (void);


/**
 * Obtain namestore handle.
 *
 * @return handle to the namestore.
 */
struct GNUNET_NAMESTORE_Handle *
GCG_IMPORT_get_namestore (void);


/**
 * Initialize the import subsystem.
 */
void
GCG_IMPORT_init (void);


/**
 * Shutdown the import subsystem.
 */
void
GCG_IMPORT_shutdown (void);

#endif
