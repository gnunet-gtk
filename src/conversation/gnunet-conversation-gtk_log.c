/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_log.c
 * @brief logging-related functions for gnunet-conversation-gtk
 * @author yids
 * @author hark
 * @author Christian Grothoff
 */
#include "gnunet-conversation-gtk.h"
#include "gnunet-conversation-gtk_log.h"


/**
 * Log a message to gtk log textbuffer
 *
 * @param message format string for message to be logged
 * @param ... arguments for the format string
 */
void
GCG_log (const char *message, ...)
{
  GtkTextBuffer *logbuff;
  GtkTextView *log_view;
  GtkTextIter iter;
  gchar *fmsg;
  va_list ap;

  log_view =
    GTK_TEXT_VIEW (GCG_get_main_window_object ("gnunet_conversation_gtk_log"));
  if (NULL == log_view)
    return;
  logbuff = GTK_TEXT_BUFFER (gtk_text_view_get_buffer (log_view));
  va_start (ap, message);
  fmsg = g_strdup_vprintf (message, ap);
  va_end (ap);
  gtk_text_buffer_get_start_iter (logbuff, &iter);
  gtk_text_buffer_insert (logbuff, &iter, fmsg, -1);
  g_free (fmsg);
}


/**
 * Update status bar.
 *
 * @param message format string for message to put in statusbar
 * @param ... arguments for the format string
 */
void
GCG_update_status_bar (const gchar *message, ...)
{
  GtkStatusbar *status_bar;
  guint status_bar_context;
  gchar *buff;
  va_list ap;

  status_bar = GTK_STATUSBAR (
    GCG_get_main_window_object ("gnunet_conversation_gtk_statusbar"));
  status_bar_context = gtk_statusbar_get_context_id (status_bar,
                                                     "blaat"); // blaat!?
  va_start (ap, message);
  buff = g_strdup_vprintf (message, ap);
  va_end (ap);
  gtk_statusbar_push (GTK_STATUSBAR (status_bar),
                      GPOINTER_TO_INT (status_bar_context),
                      buff);
  g_free (buff);
}

/* end of gnunet-conversation-gtk_log.c */
