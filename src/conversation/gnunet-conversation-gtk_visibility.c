/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_visibility.c
 * @brief show or hide widgets based on view options
 * @author Christian Grothoff
 */
#include "gnunet-conversation-gtk.h"


/**
 * Toggle the visibility of a widget based on the checkeness
 * of a menu item.
 *
 * @param toggled_widget name of widget to toggle
 * @param toggle_menu name of menu entry
 */
static void
toggle_view (const char *toggled_widget, const char *toggle_menu)
{
  GtkCheckMenuItem *mi;
  GtkWidget *widget;

  widget = GTK_WIDGET (GCG_get_main_window_object (toggled_widget));
  mi = GTK_CHECK_MENU_ITEM (GCG_get_main_window_object (toggle_menu));
  if (gtk_check_menu_item_get_active (mi))
    gtk_widget_show (widget);
  else
    gtk_widget_hide (widget);
}


/**
 * Log view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
gnunet_conversation_gtk_view_log_checkmenuitem_toggled_cb (GtkWidget *dummy,
                                                           gpointer data)
{
  toggle_view ("gnunet_conversation_gtk_log_frame",
               "gnunet_conversation_gtk_view_log_checkmenuitem");
}


/**
 * History view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
gnunet_conversation_gtk_view_history_checkmenuitem_toggled_cb (GtkWidget *dummy,
                                                               gpointer data)
{
  toggle_view ("gnunet_conversation_gtk_history_frame",
               "gnunet_conversation_gtk_view_history_checkmenuitem");
}


/**
 * Caller identity view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
gnunet_conversation_gtk_view_caller_id_checkmenuitem_toggled_cb (
  GtkWidget *dummy,
  gpointer data)
{
  toggle_view ("gnunet_conversation_gtk_caller_identity_frame",
               "gnunet_conversation_gtk_view_caller_id_checkmenuitem");
}


/**
 * Address book view is toggled.
 *
 * @param dummy widget triggering the event
 * @param data main window builder (unused)
 */
void
gnunet_conversation_gtk_view_address_book_checkmenuitem_toggled_cb (
  GtkWidget *dummy,
  gpointer data)
{
  toggle_view ("gnunet_conversation_gtk_addressbook_frame",
               "gnunet_conversation_gtk_view_address_book_checkmenuitem");
}

/* end of gnunet-conversation-gtk_visibility.c */
