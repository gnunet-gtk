/*
     This file is part of GNUnet
     Copyright (C) 2013 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/identity/gnunet-identity-gtk.h
 * @author Christian Grothoff
 */
#ifndef GNUNET_CONVERSATION_GTK_H
#define GNUNET_CONVERSATION_GTK_H

#include <stdbool.h>
#include <stdint.h>
#include "gnunet_gtk.h"
#include <gnunet/gnunet_identity_service.h>
#include <gnunet/gnunet_namestore_service.h>
#include <gnunet/gnunet_conversation_service.h>
#include <gnunet/gnunet_speaker_lib.h>
#include <gnunet/gnunet_microphone_lib.h>
#include <gnunet/gnunet_identity_service.h>
#include <gnunet/gnunet_namestore_service.h>
#include <gnunet/gnunet_gnsrecord_lib.h>


/**
 * Get our GNUnet configuration.
 *
 * @return configuration handle
 */
const struct GNUNET_CONFIGURATION_Handle *
GCG_get_configuration (void);


/**
 * Get an object from the main window.
 *
 * @param name name of the object
 * @return NULL on error
 */
GObject *
GCG_get_main_window_object (const char *name);


/**
 * update status bar
 *
 * @param message format string for message to put in statusbar
 * @param ... arguments for the format string
 */
void
GCG_update_status (const gchar *message, ...);


/**
 * log a message to gtk log textbuffer
 *
 * @param message format string for message to be logged
 * @param ... arguments for the format string
 */
void
GCG_log (const char *message, ...);

#endif
