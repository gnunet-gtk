/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk.c
 * @brief Main function of gnunet-conversation-gtk
 * @author yids
 * @author hark
 * @author Christian Grothoff
 */
#include "gnunet-conversation-gtk.h"
#include "gnunet-conversation-gtk_contacts.h"
#include "gnunet-conversation-gtk_egos.h"
#include "gnunet-conversation-gtk_history.h"
#include "gnunet-conversation-gtk_import.h"
#include "gnunet-conversation-gtk_phone.h"
#include "gnunet-conversation-gtk_zones.h"


/**
 * Handle to our main loop.
 */
static struct GNUNET_GTK_MainLoop *ml;

/**
 * Our configuration.
 */
static struct GNUNET_CONFIGURATION_Handle *cfg;

/**
 * Desired phone line.
 */
static unsigned int line;

/**
 * Ego the user wants to use.
 */
static char *ego_name;


/**
 * Get an object from the main window.
 *
 * @param name name of the object
 * @return NULL on error
 */
GObject *
GCG_get_main_window_object (const char *name)
{
  if (NULL == ml)
    return NULL;
  return GNUNET_GTK_main_loop_get_object (ml, name);
}


/**
 * Get our GNUnet configuration.
 *
 * @return configuration handle
 */
const struct GNUNET_CONFIGURATION_Handle *
GCG_get_configuration ()
{
  return GNUNET_GTK_main_loop_get_gnunet_configuration (ml);
}


/**
 * Task run on shutdown.
 *
 * @param cls unused
 */
static void
shutdown_task (void *cls)
{
  GCG_EGOS_shutdown ();
  GCG_ZONES_shutdown ();
  GCG_PHONE_shutdown ();
  GCG_CONTACTS_shutdown ();
  GCG_IMPORT_shutdown ();
  GNUNET_GTK_main_loop_quit (ml);
  ml = NULL;
}


/**
 * Callback invoked if the application is supposed to exit.
 *
 * @param object
 * @param user_data unused
 */
void
gnunet_conversation_gtk_quit_cb (GObject *object, gpointer user_data)
{
  GNUNET_SCHEDULER_shutdown ();
}


/**
 * Actual main function run right after GNUnet's scheduler
 * is initialized.  Initializes up GTK and Glade.
 *
 * @param cls NULL
 */
static void
run (void *cls)
{
  GtkWidget *main_window;

  ml = cls;
  if (GNUNET_OK !=
      GNUNET_GTK_main_loop_build_window (GNUNET_GTK_project_data (),
                                         ml,
                                         NULL))
    return;
  cfg = GNUNET_CONFIGURATION_dup (
    GNUNET_GTK_main_loop_get_gtk_configuration (ml));
  if (0 != line)
    GNUNET_CONFIGURATION_set_value_number (cfg,
                                           "CONVERSATION",
                                           "LINE",
                                           line);
  GNUNET_GTK_setup_nls ();
  /* setup main window */
  main_window = GTK_WIDGET (
    GCG_get_main_window_object ("gnunet_conversation_gtk_main_window"));
  gtk_window_maximize (GTK_WINDOW (main_window));
  /* make GUI visible */
  gtk_widget_show (main_window);
  gtk_window_present (GTK_WINDOW (main_window));
  GNUNET_SCHEDULER_add_shutdown (&shutdown_task, NULL);
  GCG_HISTORY_init ();
  GCG_ZONES_init (ego_name);
  GCG_EGOS_init (ego_name);
  GCG_IMPORT_init ();
  GCG_CONTACTS_init ();
  GCG_PHONE_init ();
  if (NULL != ego_name)
  {
    GNUNET_free (ego_name);
    ego_name = NULL;
  }
}


/**
 * Main function of gnunet-conversation-gtk.
 *
 * @param argc number of arguments
 * @param argv arguments
 * @return 0 on success
 */
int
main (int argc, char *const *argv)
{
  struct GNUNET_GETOPT_CommandLineOption options[] = {
    GNUNET_GETOPT_option_uint ('p',
                               "phone",
                               "LINE",
                               gettext_noop (
                                 "sets the LINE to use for the phone"),
                               &line),
    GNUNET_GETOPT_option_string ('e',
                                 "ego",
                                 "EGO",
                                 gettext_noop ("select ego to use"),
                                 &ego_name),
    GNUNET_GETOPT_OPTION_END
  };
  int ret;

  if (GNUNET_OK !=
      GNUNET_GTK_main_loop_start (GNUNET_GTK_project_data (),
                                  "gnunet-conversation-gtk",
                                  "GTK GUI for conversation",
                                  argc,
                                  argv,
                                  options,
                                  "gnunet_conversation_gtk_main_window.glade",
                                  &run))
    ret = 1;
  else
    ret = 0;
  if (NULL != cfg)
  {
    GNUNET_CONFIGURATION_destroy (cfg);
    cfg = NULL;
  }
  return ret;
}


/* end of gnunet-conversation-gtk.c */
