/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_history.h
 * @brief manages the call history view
 * @author yids
 * @author hark
 */
#ifndef GNUNET_CONVERSATION_GTK_HISTORY_H
#define GNUNET_CONVERSATION_GTK_HISTORY_H


/**
 * Event types used by the history.
 */
enum GCG_HISTORY_Type
{
  /**
   * We initiated an outgoing call.
   */
  GCG_HISTORY_TYPE_OUTGOING_CALL,

  /**
   * We are ringing other party in outgoing call.
   */
  GCG_HISTORY_TYPE_OUTGOING_RINGING,

  /**
   * An outgoing call was accepted.
   */
  GCG_HISTORY_TYPE_OUTGOING_ACCEPTED,

  /**
   * We have an incoming call.
   */
  GCG_HISTORY_TYPE_INCOMING_CALL,

  /**
   * We answered an incoming call.
   */
  GCG_HISTORY_TYPE_INCOMING_ACCEPTED,

  /**
   * We rejected an incoming call.
   */
  GCG_HISTORY_TYPE_INCOMING_REJECTED,

  /**
   * We missed a call (phone was ringing and
   * then caller hung up before we did anything).
   */
  GCG_HISTORY_TYPE_INCOMING_MISSED,

  /**
   * We suspended an active call.
   */
  GCG_HISTORY_TYPE_SUSPEND_LOCAL,

  /**
   * We resumed a suspended call.
   */
  GCG_HISTORY_TYPE_RESUMED_LOCAL,

  /**
   * Remote suspended an active call.
   */
  GCG_HISTORY_TYPE_SUSPEND_REMOTE,

  /**
   * Remote resumed suspended call.
   */
  GCG_HISTORY_TYPE_RESUMED_REMOTE,

  /**
   * Either side terminated an active call.
   */
  GCG_HISTORY_TYPE_HANGUP,

  /**
   * Failed to resolve target.
   */
  GCG_HISTORY_TYPE_FAILED

};


/**
 * Add an entry to the history.
 *
 * @param type type of the event
 * @param contact_name name of the contact person
 * @param call_number unique number for the call
 */
void
GCG_HISTORY_add (enum GCG_HISTORY_Type type,
                 const char *contact_name,
                 guint call_number);


/**
 * Intialize history subsystem.
 */
void
GCG_HISTORY_init (void);


#endif
