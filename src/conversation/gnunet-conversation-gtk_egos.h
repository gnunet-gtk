/*
     This file is part of GNUnet.
     Copyright (C) 2010-2014 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/conversation/gnunet-conversation-gtk_egos.h
 * @brief Manages the list of egos for selecting our caller-ID
 * @author yids
 * @author hark
 * @author Christian Grothoff
 */
#ifndef GNUNET_CONVERSATION_GTK_EGOS_H
#define GNUNET_CONVERSATION_GTK_EGOS_H


/**
 * Obtain the currently selected ego.
 *
 * @return NULL if no ego is selected
 */
struct GNUNET_IDENTITY_Ego *
GCG_EGOS_get_selected_ego (void);


/**
 * Initialize the contact list.
 *
 * @param ego_name default ego to pre-select
 */
void
GCG_EGOS_init (const char *ego_name);


/**
 * Shutdown the contact list.
 */
void
GCG_EGOS_shutdown (void);

#endif
