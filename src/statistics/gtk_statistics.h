/*
     This file is part of GNUnet.
     Copyright (C) 2010, 2011 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.
*/

/**
 * @file src/statistics/gtk_statistics.h
 * @brief widget to display statistics
 * @author Christian Grothoff
 */
#ifndef GTK_STATISTICS_H
#define GTK_STATISTICS_H

#include <stdint.h>
#include <gtk/gtk.h>
#include <cairo.h>

G_BEGIN_DECLS
#define GTK_TYPE_STATISTICS (gtk_statistics_get_type ())
#define GTK_STATISTICS(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTK_TYPE_STATISTICS, GtkStatistics))
#define GTK_STATISTICS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GTK_TYPE_STATISTICS, GtkStatisticsClass))
#define GTK_IS_STATISTICS(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTK_TYPE_STATISTICS))
#define GTK_IS_STATISTICS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTK_TYPE_STATISTICS))
#define GTK_STATISTICS_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), GTK_TYPE_STATISTICS, GtkStatisticsClass))
typedef struct _GtkStatistics GtkStatistics;
typedef struct _GtkStatisticsPrivate GtkStatisticsPrivate;
typedef struct _GtkStatisticsClass GtkStatisticsClass;

struct _GtkStatistics
{
  GtkWidget widget;

  /*< private > */
  GtkStatisticsPrivate *priv;
};

struct _GtkStatisticsClass
{
  GtkWidgetClass parent_class;

  /* Padding for future expansion */
  void (*_gtk_reserved1) (void);
  void (*_gtk_reserved2) (void);
  void (*_gtk_reserved3) (void);
  void (*_gtk_reserved4) (void);
};


GType
gtk_statistics_get_type (void) G_GNUC_CONST;
GtkWidget *
gtk_statistics_new (void);
void
gtk_statistics_add_line (GtkStatistics *statistics,
                         const char *id,
                         const char *label,
                         const char *color_name);
void
gtk_statistics_update_value (GtkStatistics *statistics,
                             const char *id,
                             uint64_t x,
                             uint64_t y);


G_END_DECLS
#endif
