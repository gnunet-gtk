/*
     This file is part of GNUnet
     Copyright (C) 2004, 2005 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.

*/

#ifndef STATS_FUNCTIONS_H
#define STATS_FUNCTIONS_H

#include <GNUnet/gnunet_util.h>

typedef struct
{
  char *statName;
  unsigned long long value;
  unsigned long long lvalue;
  GNUNET_CronTime delta;
} StatPair;

/**
 * Callback function to obtain the latest stats
 * data for this stat display.
 */
typedef int (*UpdateData) (const void *closure, gfloat **data);


typedef struct SE_
{
  char *paneName;
  char *frameName;
  UpdateData getData;
  void *get_closure;
  unsigned int count;
  unsigned int fill; /* fill first "fill" entries */
} StatEntry;

extern StatEntry stats[];

void
init_functions (struct GNUNET_GE_Context *e, struct GNUNET_GC_Configuration *c);

void
done_functions (void);

#endif
