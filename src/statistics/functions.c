/*
     This file is part of GNUnet
     Copyright (C) 2004, 2005, 2006, 2008 GNUnet e.V.

     GNUnet is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published
     by the Free Software Foundation; either version 3, or (at your
     option) any later version.

     GNUnet is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with GNUnet; see the file COPYING.  If not, write to the
     Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
     Boston, MA 02110-1301, USA.

*/

#include "gnunetgtk_common.h"
#include <GNUnet/gnunet_util_cron.h>
#include <GNUnet/gnunet_stats_lib.h>
#include <GNUnet/gnunet_getoption_lib.h>
#include <GNUnet/gnunet_protocols.h>
#include "functions.h"

#define FUNCTIONS_DEBUG GNUNET_NO

static StatPair *lastStatValues;

static unsigned int lsv_size;

static struct GNUNET_ClientServerConnection *sock;

static struct GNUNET_Mutex *lock;

static long connectionGoal;

static unsigned long long banddown;

static unsigned long long bandup;

static struct GNUNET_GE_Context *ectx;

static struct GNUNET_GC_Configuration *cfg;

static struct GNUNET_CronManager *cron;

static int
getStatValue (unsigned long long *value,
              unsigned long long *lvalue,
              GNUNET_CronTime *dtime,
              const char *optName,
              int monotone)
{
  unsigned int i;

  *value = 0;
  if (lvalue != NULL)
    *lvalue = 0;
  for (i = 0; i < lsv_size; i++)
  {
    if (0 == strcmp (optName, lastStatValues[i].statName))
    {
      *value = lastStatValues[i].value;
      if (lvalue != NULL)
        *lvalue = lastStatValues[i].lvalue;
      if (dtime != NULL)
        *dtime = lastStatValues[i].delta;
      if ((monotone == GNUNET_YES) && (lvalue != NULL) && (*lvalue > *value))
        return GNUNET_SYSERR; /* gnunetd restart? */
      return GNUNET_OK;
    }
  }
#if FUNCTIONS_DEBUG
  GNUNET_GE_LOG (ectx,
                 GNUNET_GE_ERROR | GNUNET_GE_DEVELOPER | GNUNET_GE_ADMIN |
                   GNUNET_GE_USER | GNUNET_GE_BULK,
                 "Statistic not found: `%s'\n",
                 optName);
#endif
  return GNUNET_SYSERR;
}

static void
updateConnectionGoal (void *unused)
{
  char *cmh;
  char *availableDown;
  char *availableUp;

  GNUNET_mutex_lock (lock);
  cmh = GNUNET_get_daemon_configuration_value (sock,
                                               "gnunetd",
                                               "connection-max-hosts");
  availableDown =
    GNUNET_get_daemon_configuration_value (sock, "LOAD", "MAXNETDOWNBPSTOTAL");
  availableUp =
    GNUNET_get_daemon_configuration_value (sock, "LOAD", "MAXNETUPBPSTOTAL");
  GNUNET_mutex_unlock (lock);
  if (cmh == NULL)
    connectionGoal = 0;
  else
    connectionGoal = atol (cmh);
  if (availableDown == NULL)
    banddown = 0;
  else
    banddown = atol (availableDown);
  if (availableUp == NULL)
    bandup = 0;
  else
    bandup = atol (availableUp);

  GNUNET_free (cmh);
  GNUNET_free (availableDown);
  GNUNET_free (availableUp);
}

static int
getConnectedNodesStat (const void *closure, gfloat **data)
{
  unsigned long long val;

  if (connectionGoal == 0)
    return GNUNET_SYSERR;
  if (GNUNET_OK !=
      getStatValue (&val, NULL, NULL, "# of connected peers", GNUNET_NO))
    return GNUNET_SYSERR;
  data[0][0] = ((gfloat) val) / connectionGoal;
  return GNUNET_OK;
}

static int
getLoadStat (const void *closure, gfloat **data)
{
  unsigned long long valc;
  unsigned long long vali;
  unsigned long long valu;
  unsigned long long vald;

  if (GNUNET_OK !=
      getStatValue (&valc, NULL, NULL, "% of allowed cpu load", GNUNET_NO))
    return GNUNET_SYSERR;
  if (GNUNET_OK !=
      getStatValue (&vali, NULL, NULL, "% of allowed io load", GNUNET_NO))
    return GNUNET_SYSERR;
  if (GNUNET_OK != getStatValue (&valu,
                                 NULL,
                                 NULL,
                                 "% of allowed network load (up)",
                                 GNUNET_NO))
    return GNUNET_SYSERR;
  if (GNUNET_OK != getStatValue (&vald,
                                 NULL,
                                 NULL,
                                 "% of allowed network load (down)",
                                 GNUNET_NO))
    return GNUNET_SYSERR;
  data[0][0] = (gfloat) valc / 100.0;
  data[0][1] = (gfloat) vali / 100.0;
  data[0][2] = (gfloat) valu / 100.0;
  data[0][3] = (gfloat) vald / 100.0;
  return GNUNET_OK;
}

static int
getQuotaStat (const void *closure, gfloat **data)
{
  unsigned long long allowed;
  unsigned long long have;

  data[0][0] = 0;
  data[0][1] = 0;
  if ((GNUNET_OK == getStatValue (&allowed,
                                  NULL,
                                  NULL,
                                  "# bytes allowed in datastore",
                                  GNUNET_NO)) &&
      (allowed != 0) &&
      (GNUNET_OK ==
       getStatValue (&have, NULL, NULL, "# bytes in datastore", GNUNET_NO)))
    data[0][0] = ((gfloat) have) / allowed;
  if ((GNUNET_OK == getStatValue (&allowed,
                                  NULL,
                                  NULL,
                                  "# max bytes allowed in dstore",
                                  GNUNET_NO)) &&
      (allowed != 0) &&
      (GNUNET_OK ==
       getStatValue (&have, NULL, NULL, "# bytes in dstore", GNUNET_NO)))
    data[0][1] = ((gfloat) have) / allowed;
  return GNUNET_OK;
}

static int
getTrafficRecvStats (const void *closure, gfloat **data)
{
  unsigned long long total;
  unsigned long long noise;
  unsigned long long content;
  unsigned long long queries;
  unsigned long long hellos;
  unsigned long long rlimit;
  unsigned long long ltotal;
  unsigned long long lnoise;
  unsigned long long lcontent;
  unsigned long long lqueries;
  unsigned long long lhellos;
  unsigned long long lrlimit;
  GNUNET_CronTime dtime;
  char *buffer;

  if (GNUNET_OK !=
      getStatValue (&total, &ltotal, &dtime, "# bytes received", GNUNET_YES))
    return GNUNET_SYSERR;
  if (GNUNET_OK != getStatValue (&noise,
                                 &lnoise,
                                 NULL,
                                 "# bytes of noise received",
                                 GNUNET_YES))
    return GNUNET_SYSERR;
  buffer = GNUNET_malloc (512);
  GNUNET_snprintf (buffer,
                   512,
                   "# bytes received of type %d",
                   GNUNET_P2P_PROTO_GAP_RESULT);
  if (GNUNET_OK != getStatValue (&content, &lcontent, NULL, buffer, GNUNET_YES))
  {
    content = 0;
    lcontent = 0;
  }
  GNUNET_snprintf (buffer,
                   512,
                   "# bytes received of type %d",
                   GNUNET_P2P_PROTO_HELLO);
  if (GNUNET_OK != getStatValue (&hellos, &lhellos, NULL, buffer, GNUNET_YES))
  {
    hellos = 0;
    lhellos = 0;
  }
  GNUNET_snprintf (buffer,
                   512,
                   "# bytes received of type %d",
                   GNUNET_P2P_PROTO_GAP_QUERY);
  if (GNUNET_OK != getStatValue (&queries, &lqueries, NULL, buffer, GNUNET_YES))
  {
    queries = 0;
    lqueries = 0;
  }
  if (GNUNET_OK != getStatValue (&rlimit,
                                 &lrlimit,
                                 NULL,
                                 "# total bytes per second receive limit",
                                 GNUNET_NO))
  {
    rlimit = 0;
    lrlimit = 0;
  }
  GNUNET_free (buffer);
  if (banddown == 0)
    return GNUNET_SYSERR;

  total -= ltotal;
  noise -= lnoise;
  queries -= lqueries;
  content -= lcontent;
  hellos -= lhellos;
  if (banddown <= 0)
  {
    data[0][0] = 0.0;
    data[0][1] = 0.0;
    data[0][2] = 0.0;
    data[0][3] = 0.0;
    data[0][4] = 0.0;
    data[0][5] = 0.0;
    return GNUNET_OK;
  }
  data[0][0] =
    ((gfloat) noise) / (banddown * dtime / GNUNET_CRON_SECONDS); /* red */
  data[0][1] = ((gfloat) (content + noise)) /
               (banddown * dtime / GNUNET_CRON_SECONDS); /* green */
  data[0][2] = ((gfloat) (queries + content + noise)) /
               (banddown * dtime / GNUNET_CRON_SECONDS); /* yellow */
  data[0][3] = ((gfloat) (queries + content + noise + hellos)) /
               (banddown * dtime / GNUNET_CRON_SECONDS); /* blue */
  data[0][4] =
    ((gfloat) total) / (banddown * dtime / GNUNET_CRON_SECONDS); /* gray */
  data[0][5] = (gfloat) rlimit / banddown; /* magenta */
#if 0
  printf ("I: %f %f %f %f\n", data[0][0], data[0][1], data[0][2]);
#endif
  return GNUNET_OK;
}

static int
getTrafficSendStats (const void *closure, gfloat **data)
{
  unsigned long long total;
  unsigned long long noise;
  unsigned long long content;
  unsigned long long queries;
  unsigned long long hellos;
  unsigned long long slimit;
  unsigned long long ltotal;
  unsigned long long lnoise;
  unsigned long long lcontent;
  unsigned long long lqueries;
  unsigned long long lhellos;
  unsigned long long lslimit;
  GNUNET_CronTime dtime;
  char *buffer;

  if (GNUNET_OK !=
      getStatValue (&total, &ltotal, &dtime, "# bytes transmitted", GNUNET_YES))
    return GNUNET_SYSERR;
  if (GNUNET_OK !=
      getStatValue (&noise, &lnoise, NULL, "# bytes noise sent", GNUNET_YES))
    return GNUNET_SYSERR;
  buffer = GNUNET_malloc (512);
  GNUNET_snprintf (buffer,
                   512,
                   "# bytes transmitted of type %d",
                   GNUNET_P2P_PROTO_GAP_RESULT);
  if (GNUNET_OK != getStatValue (&content, &lcontent, NULL, buffer, GNUNET_YES))
  {
    content = 0;
    lcontent = 0;
  }
  GNUNET_snprintf (buffer,
                   512,
                   "# bytes transmitted of type %d",
                   GNUNET_P2P_PROTO_GAP_QUERY);
  if (GNUNET_OK != getStatValue (&queries, &lqueries, NULL, buffer, GNUNET_YES))
  {
    queries = 0;
    lqueries = 0;
  }
  GNUNET_snprintf (buffer,
                   512,
                   "# bytes transmitted of type %d",
                   GNUNET_P2P_PROTO_HELLO);
  if (GNUNET_OK != getStatValue (&hellos, &lhellos, NULL, buffer, GNUNET_YES))
  {
    queries = 0;
    lqueries = 0;
  }
  if (GNUNET_OK != getStatValue (&slimit,
                                 &lslimit,
                                 NULL,
                                 "# total bytes per second send limit",
                                 GNUNET_NO))
  {
    slimit = 0;
    lslimit = 0;
  }
  GNUNET_free (buffer);
  if (bandup == 0)
    return GNUNET_SYSERR;
  total -= ltotal;
  noise -= lnoise;
  queries -= lqueries;
  content -= lcontent;
  hellos -= lhellos;
  if (bandup <= 0)
  {
    data[0][0] = 0.0;
    data[0][1] = 0.0;
    data[0][2] = 0.0;
    data[0][3] = 0.0;
    data[0][4] = 0.0;
    data[0][5] = 0.0;
    return GNUNET_OK;
  }
  data[0][0] =
    ((gfloat) noise) / (bandup * dtime / GNUNET_CRON_SECONDS); /* red */
  data[0][1] = ((gfloat) (noise + content)) /
               (bandup * dtime / GNUNET_CRON_SECONDS); /* green */
  data[0][2] = ((gfloat) (noise + content + queries)) /
               (bandup * dtime / GNUNET_CRON_SECONDS); /* yellow */
  data[0][3] = ((gfloat) (noise + content + queries + hellos)) /
               (bandup * dtime / GNUNET_CRON_SECONDS); /* blue */
  data[0][4] =
    ((gfloat) total) / (bandup * dtime / GNUNET_CRON_SECONDS); /* grey */
  data[0][5] = ((gfloat) slimit) / bandup; /* magenta */
#if 0
  printf ("O: %f %f %f %f\n", data[0][0], data[0][1], data[0][2], data[0][3]);
#endif
  return GNUNET_OK;
}

static int
getTrustStats (const void *closure, gfloat **data)
{
  unsigned long long spent;
  unsigned long long earned;
  unsigned long long awarded;
  unsigned long long lspent;
  unsigned long long learned;
  unsigned long long lawarded;
  unsigned long long max;

  if (GNUNET_OK !=
      getStatValue (&spent, &lspent, NULL, "# trust spent", GNUNET_YES))
    return GNUNET_SYSERR;
  if (GNUNET_OK !=
      getStatValue (&earned, &learned, NULL, "# trust earned", GNUNET_YES))
    return GNUNET_SYSERR;
  if (GNUNET_OK != getStatValue (&awarded,
                                 &lawarded,
                                 NULL,
                                 "# gap total trust awarded",
                                 GNUNET_YES))
    return GNUNET_SYSERR;
  max = spent;
  if (earned > max)
    max = earned;
  if (awarded > max)
    max = awarded;
  data[0][0] = 0.0;
  if (max > 0)
  {
    data[0][0] = (1.0 * spent) / max;
    data[0][1] = (1.0 * earned) / max;
    data[0][2] = (1.0 * awarded) / max;
  }
  else
  {
    data[0][0] = 0.0;
    data[0][1] = 0.0;
    data[0][2] = 0.0;
  }
  return GNUNET_OK;
}

static int
getEffectivenessStats (const void *closure, gfloat **data)
{
  unsigned long long total; /* total number of queries passed on to remote */
  unsigned long long
    success; /* responses forwarded (including local and remote) */
  unsigned long long ltotal;
  unsigned long long lsuccess;

  if (GNUNET_OK != getStatValue (&total,
                                 &ltotal,
                                 NULL,
                                 "# gap requests total sent",
                                 GNUNET_YES))
    return GNUNET_SYSERR;
  if (GNUNET_OK != getStatValue (&success,
                                 &lsuccess,
                                 NULL,
                                 "# gap routes succeeded",
                                 GNUNET_YES))
    return GNUNET_SYSERR;
  if (total > 0)
    data[0][0] = 1.0 * success / total;
  else
    data[0][0] = 0.0;
  return GNUNET_OK;
}


static int
statsProcessor (const char *optName, unsigned long long value, void *data)
{
  /**
   * Keep track of last match (or, more precisely, position
   * of next expected match) since 99.99% of the time we
   * go over the same stats in the same order and thus
   * this will predict correctly).
   */
  static unsigned int last;
  GNUNET_CronTime *delta = data;
  unsigned int j;
  unsigned int found;

  if (last >= lsv_size)
    last = 0;
  j = last;
  found = -1;
  if ((j < lsv_size) && (0 == strcmp (optName, lastStatValues[j].statName)))
    found = j;
  if (found == (unsigned int) -1)
  {
    for (j = 0; j < lsv_size; j++)
    {
      if (0 == strcmp (optName, lastStatValues[j].statName))
      {
        found = j;
        break;
      }
    }
  }
  if (found == (unsigned int) -1)
  {
    found = lsv_size;
    GNUNET_array_grow (lastStatValues, lsv_size, lsv_size + 1);
    lastStatValues[found].statName = GNUNET_strdup (optName);
  }
  lastStatValues[found].lvalue = lastStatValues[found].value;
  lastStatValues[found].value = value;
  lastStatValues[found].delta = *delta;
  last = found + 1;
  return GNUNET_OK;
}

struct UpdateClosure
{
  GNUNET_CronTime delta;
  int is_running;
};

/*
 * Update the status bar indicator about daemon and connexions status
 */
static void *
updateDaemonStatus (void *cls)
{
  struct UpdateClosure *uc = cls;
  unsigned long long connected_peers;

  if (uc->is_running)
  {
    if (GNUNET_OK != getStatValue (&connected_peers,
                                   NULL,
                                   NULL,
                                   "# of connected peers",
                                   GNUNET_NO))
      GNUNET_GTK_display_daemon_status (GNUNET_GTK_STATUS_UNKNOWN, 0);
    else if (connected_peers > 0)
      GNUNET_GTK_display_daemon_status (GNUNET_GTK_STATUS_CONNECTED,
                                        connected_peers);
    else
      GNUNET_GTK_display_daemon_status (GNUNET_GTK_STATUS_DISCONNECTED, 0);
  }
  else
    GNUNET_GTK_display_daemon_status (GNUNET_GTK_STATUS_NODAEMON, 0);
  return NULL;
}

/**
 * Cron-job that updates all stat values.
 */
static void
updateStatValues (void *unused)
{
  static GNUNET_CronTime lastUpdate;
  GNUNET_CronTime now;
  struct UpdateClosure uc;

  now = GNUNET_get_time ();
  uc.delta = now - lastUpdate;
  uc.is_running = GNUNET_OK == GNUNET_test_daemon_running (ectx, cfg);
  GNUNET_mutex_lock (lock);
  if (GNUNET_OK ==
      GNUNET_STATS_get_statistics (ectx, sock, &statsProcessor, &uc.delta))
    lastUpdate = now;
  GNUNET_GTK_save_call (&updateDaemonStatus, (void *) &uc);
  GNUNET_mutex_unlock (lock);
}


StatEntry stats[] = {
  {
    gettext_noop ("Connectivity"),
    gettext_noop ("# connected nodes (100% = connection table size)"),
    &getConnectedNodesStat,
    NULL,
    1,
    GNUNET_NO,
  },
  {
    gettext_noop ("System load"),
    gettext_noop (
      "CPU load (red), IO load (green), Network upload (yellow), Network download (blue)"),
    &getLoadStat,
    NULL,
    4,
    GNUNET_NO,
  },
  {
    gettext_noop ("Datastore capacity"),
    gettext_noop ("Persistent file-sharing data (red) and DHT cache (green)"),
    &getQuotaStat,
    NULL,
    2,
    GNUNET_NO,
  },
  {
    gettext_noop ("Inbound Traffic"),
    gettext_noop (
      "Noise (red), Content (green), Queries (yellow), Hellos (blue), other (gray), limit (magenta)"),
    &getTrafficRecvStats,
    NULL,
    6,
    5,
  },
  {
    gettext_noop ("Outbound Traffic"),
    gettext_noop (
      "Noise (red), Content (green), Queries (yellow), Hellos (blue), other (gray), limit (magenta)"),
    &getTrafficSendStats,
    NULL,
    6,
    5,
  },
  {
    gettext_noop ("Trust"),
    gettext_noop ("Spent (red), Earned (green) and Awarded (yellow)"),
    &getTrustStats,
    NULL,
    3,
    GNUNET_NO,
  },
  {
    gettext_noop ("Routing Effectiveness"),
    gettext_noop ("Average (red) effectiveness (100% = perfect)"),
    &getEffectivenessStats,
    NULL,
    1,
    GNUNET_NO,
  },
  {
    NULL,
    NULL,
    NULL,
    NULL,
    0,
    GNUNET_NO,
  },
};

static unsigned long long UPDATE_INTERVAL;

void
init_functions (struct GNUNET_GE_Context *e, struct GNUNET_GC_Configuration *c)
{
  ectx = e;
  cfg = c;
  GNUNET_GC_get_configuration_value_number (cfg,
                                            "GNUNET-GTK",
                                            "STATS-INTERVAL",
                                            1,
                                            99 * GNUNET_CRON_YEARS,
                                            30 * GNUNET_CRON_SECONDS,
                                            &UPDATE_INTERVAL);
  sock = GNUNET_client_connection_create (ectx, cfg);
  lock = GNUNET_mutex_create (GNUNET_NO);
  cron = GNUNET_GTK_get_cron_manager ();
  GNUNET_cron_add_job (cron,
                       &updateStatValues,
                       UPDATE_INTERVAL,
                       UPDATE_INTERVAL,
                       NULL);
  GNUNET_cron_add_job (cron,
                       &updateConnectionGoal,
                       5 * GNUNET_CRON_SECONDS,
                       5 * GNUNET_CRON_MINUTES,
                       NULL);
  updateStatValues (NULL);
}

void
done_functions ()
{
  unsigned int i;

  GNUNET_cron_del_job (cron,
                       &updateConnectionGoal,
                       5 * GNUNET_CRON_MINUTES,
                       NULL);
  GNUNET_cron_del_job (cron, &updateStatValues, UPDATE_INTERVAL, NULL);
  GNUNET_mutex_destroy (lock);
  GNUNET_client_connection_destroy (sock);
  for (i = 0; i < lsv_size; i++)
    GNUNET_free (lastStatValues[i].statName);
  GNUNET_array_grow (lastStatValues, lsv_size, 0);
  sock = NULL;
}


/* end of functions.c */
